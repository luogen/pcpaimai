<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:65:"E:\phpstudy\WWW\pcpaimai/app/admin\view\web_auction\auctForm.html";i:1511853397;s:60:"E:\phpstudy\WWW\pcpaimai/app/admin\view\common\mainHead.html";i:1503968852;s:60:"E:\phpstudy\WWW\pcpaimai/app/admin\view\common\mainFoot.html";i:1503968852;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Paging</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="__ADMIN__/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="__ADMIN__/css/global.css" media="all">
    <link rel="stylesheet" href="__ADMIN__/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="__ADMIN__/css/style.css">
    <link rel="stylesheet" href="__ADMIN__/css/animate.css" />
</head>
<body>
<div class="admin-main fadeInUp animated" ng-app="hd" ng-controller="ctrl">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
        <legend><?php echo $title; ?></legend>
    </fieldset>
    <form class="layui-form layui-form-pane">
        <div class="layui-form-item">
            <label class="layui-form-label">标题</label>
            <div class="layui-input-4">
                <input type="text" name="title" ng-model="field.title" lay-verify="required" placeholder="<?php echo lang('pleaseEnter'); ?>拍卖会标题" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">选择拍卖师</label>
            <div class="layui-input-4">
                <select name="auctor_id" ng-model="field.auctor_id" ng-options="v.admin_id as v.username for v in group" ng-selected="v.admin_id==field.auctor_id">

                </select>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">起拍时间</label>
            <div class="layui-input-4" id="box_starttime">
                <input type="datetime" ng-model="field.starttime" title="起拍时间" name="starttime" data-required="1" placeholder="请输入起拍时间" lay-verify="date" value="" class=" layui-input" onclick="layui.laydate({elem: this, istime: true,festival: true,format: 'YYYY-MM-DD hh:mm:ss'})">
            </div><div class="layui-form-mid layui-word-aux red">*必填                    </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">封面图片1</label>
            <div class="layui-input-block">
                <div class="site-demo-upload">
                    <img id="cltThumb1" src="__ADMIN__/images/tong.png">
                    <div class="site-demo-upbar">
                        <input type="file" name="pic1" lay-type="images" class="layui-upload-file" id="thumb1">
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">是否发送短信</label>
            <div class="layui-input-4" id="box_status">
                <input ng-value=1 ng-model="field.status" name="status" id="status_1" value="1"  type="radio" class="ace" title="是">
                <div class="layui-unselect layui-form-radio layui-form-radioed">
                    <i class="layui-anim layui-icon"></i><span>是</span>
                </div>
                <input ng-value=0 ng-model="field.status" name="status" id="status_2" value="0" type="radio" class="ace" title="否">
                <div class="layui-unselect layui-form-radio">
                    <i class="layui-anim layui-icon"></i><span>否</span>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button type="button" class="layui-btn" lay-submit="" lay-filter="submit"><?php echo lang('submit'); ?></button>
                <a href="<?php echo url('index'); ?>" class="layui-btn layui-btn-primary"><?php echo lang('back'); ?></a>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript" src="__ADMIN__/plugins/layui/layui.js"></script>
<script src="__STATIC__/js/jquery.2.1.1.min.js"></script>
<script src="http://www.jq22.com/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="__STATIC__/js/jquery.jqprint.js"></script>
<script src="__STATIC__/js/angular.min.js"></script>

<script>
    var m = angular.module('hd',[]);
    m.controller('ctrl',['$scope',function($scope) {
        $scope.field = '<?php echo $info; ?>'!='null'?<?php echo $info; ?>:{id:'',title:'',starttime:'',thumb1:'',status:''};
        if($scope.field.starttime != ''){
            $scope.field.starttime=getdate($scope.field.starttime);
        }
        $scope.group = <?php echo $auctorList; ?>;
        layui.use(['form', 'layer','laydate','upload'], function () {
            var form = layui.form(), layer = layui.layer,laydate = layui.laydate,upload = layui.upload;
            if($scope.field.thumb1){
                cltThumb1.src = "__PUBLIC__"+ $scope.field.thumb1;
            }
            upload({
                url: '<?php echo url("UpFiles/upload"); ?>',
                title: '封面图片',
                ext: 'jpg|png|gif', //那么，就只会支持这三种格式的上传。注意是用|分割。
                success: function(res, input){
                    if(input.name == 'pic1'){
                        cltThumb1.src = "__PUBLIC__"+res.url;
                        $scope.field.thumb1 = res.url;
                    }

                }
            });
            form.on('submit(submit)', function (data) {
                // 提交到方法 默认为本身
                data.field.id = $scope.field.id;
                data.field.thumb1=$scope.field.thumb1;
                $.post("", data.field, function (res) {
                    if (res.code > 0) {
                        layer.msg(res.msg, {time: 1800, icon: 1}, function () {
                            location.href = res.url;
                        });
                    } else {
                        layer.msg(res.msg, {time: 1800, icon: 2});
                    }
                });
            })
        });
    }]);
    function getdate(date){
        var date = new Date(date*1000);//如果date为10位不需要乘1000
        var Y = date.getFullYear() + '-';
        var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
        var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
        var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
        var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
        var s = (date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds());
        return Y+M+D+h+m+s;
    }
//    layui.use(['laydate'], function () {
//
//    });
</script>