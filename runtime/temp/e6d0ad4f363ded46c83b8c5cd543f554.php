<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:60:"E:\phpstudy\WWW\pcpaimai/app/admin\view\web_goods\index.html";i:1511334247;s:60:"E:\phpstudy\WWW\pcpaimai/app/admin\view\common\mainHead.html";i:1503968852;s:60:"E:\phpstudy\WWW\pcpaimai/app/admin\view\common\mainFoot.html";i:1503968852;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Paging</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="__ADMIN__/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="__ADMIN__/css/global.css" media="all">
    <link rel="stylesheet" href="__ADMIN__/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="__ADMIN__/css/style.css">
    <link rel="stylesheet" href="__ADMIN__/css/animate.css" />
</head>
<body>
<div class="admin-main fadeInUp animated">
    <blockquote class="layui-elem-quote">
        <a href="<?php echo url('addGood'); ?>?auct_id=<?php echo $auctionid; ?>" class="layui-btn layui-btn-small">
            <i class="fa fa-plus"></i> <?php echo lang('add'); ?>拍品
        </a>
        <a href="<?php echo url('WebAuction/index'); ?>" class="layui-btn layui-btn-small">
             返回拍卖会
        </a>
    </blockquote>
    <fieldset class="layui-elem-field">
        <legend>该拍卖会下所有拍品</legend>
        <div class="layui-field-box table-responsive">
            <form class="layui-form layui-form-pane">
                <table class="layui-table table-hover">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>排序</th>
                        <th>名称</th>
                        <th>缩略图1</th>
                        <th>起拍价</th>
                        <th>加价幅度</th>
                        <th>保留价</th>
                        <th>拍卖师</th>
                        <th>自由竞价时间</th>
                        <th>先生竞价时间</th>
                        <th>创建时间</th>
                        <th>状态</th><!--未结束，如果已经结束显示流拍或者成交   if is_end=0{if is_pause==0 未开始或被暂停 else if is_pause==1} ,else isend==1 {if is_sure=0流拍 else 成交} -->
                        <th>操作</th>
                    </tr>
                    </thead>
                    <!--内容容器-->
                    <tbody id="con">
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="13">
                            <!--分页容器-->
                            <div id="paged" style="text-align: right"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="13">
                            <button type="button" class="layui-btn  layui-btn-small" lay-submit="" lay-filter="sort">排序</button>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </form>
        </div>
    </fieldset>
</div>
<!--模板-->
<script type="text/html" id="conTemp">
    {{# layui.each(d.list, function(index, item){ }}
    <tr>
        <td>{{ item.id }}</td>
        <td><input name="listorders[{{ item.id }}]" type="text" size="10" value="{{ item.sort }}" class="list_order"></td>
        <td>{{ item.title }}</td>
        <td><img src="__PUBLIC__/{{ item.thumb1 }}" style="width: 60px;height: 60px;"></td>
        <td>{{ item.start_price }}元</td>
        <td>{{ item.marup_price }}元</td>
        <td>{{ item.end_price }}元</td>
        <td>{{ item.auctineer}}</td>
        <td>{{ item.freetime}}秒</td>
        <td>{{ item.cuttime}}秒</td>
        <td>{{ getdate(item.cretime)}}</td>
        <td style="color:red;">
            {{# if(item.is_end==0){ }}
                {{# if(item.is_giveup==1){ }}
                    被放弃
                {{# }else{  }}
                    {{# if(item.is_pause==1){ }}
                        进行中
                    {{# }else{  }}
                        {{# if(item.starttime==0){ }}
                        未开始
                        {{# }else{  }}
                        被暂停
                        {{# } }}
                    {{# } }}
                {{# } }}
            {{# }else{  }}
                {{# if(item.is_sure==1){ }}
                    已流拍
                {{# }else{  }}
                    已成交
                {{# } }}
            {{# } }}
        </td>
        <td>
            <a href="<?php echo url('WebGoods/detailGood'); ?>?id={{item.id}}" class="layui-btn layui-btn-mini">进入</a>
            <a href="<?php echo url('WebGoods/editGood'); ?>?id={{item.id}}" class="layui-btn layui-btn-mini"><?php echo lang('edit'); ?></a>
            <a href="javascript:;" onclick="return del({{item.id}})" data-id="1" data-opt="del" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>
        </td>
    </tr>
    {{# }); }}
</script>
<script type="text/javascript" src="__ADMIN__/plugins/layui/layui.js"></script>
<script src="__STATIC__/js/jquery.2.1.1.min.js"></script>
<script src="http://www.jq22.com/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="__STATIC__/js/jquery.jqprint.js"></script>
<script>
        layui.config({
        base: '__ADMIN__/js/'
    }).use(['paging', 'code','icheck','layer'], function() {
        layui.code();
        var paging = layui.paging(),layer = parent.layer === undefined ? layui.layer : parent.layer;
        paging.init({
            url: "<?php echo url('index'); ?>", //地址
            elem: '#con', //内容容器
            params: {auctionid:<?php echo $auctionid; ?>}, //发送到服务端的参数
            tempElem: '#conTemp', //模块容器
            pageConfig: { //分页参数配置
                elem: '#paged', //分页容器
                pageSize: 15 //分页大小
            },
            success: function() { //渲染成功的回调
                //alert('渲染成功');
            },
            fail: function(msg) { //获取数据失败的回调
                //alert('获取数据失败')
            },
            complate: function() { //完成的回调
                //alert('处理完成');
            }
        });
    });
        function getdate(date){
            var date = new Date(date*1000);//如果date为10位不需要乘1000
            var Y = date.getFullYear() + '-';
            var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
            var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
            var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            var s = (date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y+M+D+h+m+s;
        }

        function stateyes(id) {
            $.post('<?php echo url("goodsState"); ?>', {id: id}, function (data) {
                if (data.status) {
                    if (data.info == '已撤回') {
                        var a = '<button class="layui-btn layui-btn-danger layui-btn-mini">已撤回</button>'
                        $('#zt' + id).html(a);
                        layer.msg(data.info, {icon: 5});
                        return false;
                    } else {
                        var b = '<button class="layui-btn layui-btn-warm layui-btn-mini">已发布</button>'
                        $('#zt' + id).html(b);
                        layer.msg(data.info, {icon: 6});
                        return false;
                    }
                }else{
                    layer.msg(data.msg,{time:1000,icon:2});
                    return false;
                }
            });
            return false;
        }


        function del(id) {
        layer.confirm('你确定要删除吗？', {icon: 3}, function (index) {
            layer.close(index);
            window.location.href = "<?php echo url('delGoods'); ?>?id=" + id;
        });
        }
        //排序提交
        layui.use(['form', 'layer'], function() {
            var form = layui.form(),layer = layui.layer;
            form.on('submit(sort)', function(data){
                $.post("/index.php/admin/WebGoods/cOrder.html",data.field,function(res){
                    if(res.code > 0){
                        layer.msg(res.msg,{time:1000,icon:1},function(){
                            location.href = res.url;
                        });
                    }else{
                        layer.msg(res.msg,{time:1000,icon:2});
                    }
                })
            });
        });

</script>