<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:65:"E:\phpstudy\WWW\pcpaimai/app/admin\view\web_goods\detailGood.html";i:1511168003;s:60:"E:\phpstudy\WWW\pcpaimai/app/admin\view\common\mainHead.html";i:1503968852;s:60:"E:\phpstudy\WWW\pcpaimai/app/admin\view\common\mainFoot.html";i:1503968852;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Paging</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="__ADMIN__/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="__ADMIN__/css/global.css" media="all">
    <link rel="stylesheet" href="__ADMIN__/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="__ADMIN__/css/style.css">
    <link rel="stylesheet" href="__ADMIN__/css/animate.css" />
</head>
<body>
<div class="admin-main fadeInUp animated">
    <blockquote class="layui-elem-quote">
        <a href="<?php echo url('WebQualify/index'); ?>?goodsid=<?php echo $good_info['id']; ?>" class="layui-btn layui-btn-small">
            资格审核
        </a>
        <a href="<?php echo url('WebBidden/index'); ?>?goodsid=<?php echo $good_info['id']; ?>" class="layui-btn layui-btn-small">
            出价记录
        </a>
        <a href="<?php echo url('WebAucwords/index'); ?>?goodsid=<?php echo $good_info['id']; ?> " class="layui-btn layui-btn-small">
            拍卖师发言
        </a>
        <a href="<?php echo url('WebGoods/editGood'); ?>?id=<?php echo $good_info['id']; ?>&con=op" class="layui-btn layui-btn-small">
            修改（该操作必须先暂停拍品）
        </a>
        <a href="<?php echo url('WebGoods/index'); ?>?id=<?php echo $good_info['auct_id']; ?>" class="layui-btn layui-btn-small">
            返回拍品列表
        </a>
    </blockquote>
    <fieldset class="layui-elem-field">
        <legend>拍品信息</legend>
        <div class="layui-field-box table-responsive">
            <form class="layui-form layui-form-pane">
                <table class="layui-table table-hover">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>参与人数</th>
                        <th>目前最高</th>
                        <th>状态</th>
                        <th>起拍价</th>
                        <th>加价幅度</th>
                        <th>保留价</th>
                    </tr>
                    </thead>
                    <!--内容容器-->
                    <tbody id="con">
                        <tr>
                            <td><?php echo $good_info['id']; ?></td>
                            <td><?php echo $good_info['user_num']; ?>人</td>
                            <td>￥<?php echo $good_info['highest']; ?></td>
                            <td style="color:red;">
                                <?php
                                    if($good_info['is_end'] ==0 ){
                                 if($good_info['is_giveup']==1){
                                    echo '被放弃';
                                 }else{
                                 if($good_info['is_pause']==1){
                                    echo '进行中';
                                 }else{
                                    //未开始或被暂停（这块儿不好区分）
                                    if($good_info['starttime'])//如果存在开始时间肯定是开始了的被暂停了
                                        echo '被暂停';
                                    else
                                        echo '未开始';
                                 }
                                 }
                                 }else{
                                 if($good_info['is_sure']==1){
                                    echo '已流拍';
                                 }else{
                                    echo '已成交';
                                 }
                                 }
                                ?>
                            </td>
                            <td>￥<?php echo $good_info['start_price']; ?></td>
                            <td>￥<?php echo $good_info['marup_price']; ?></td>
                            <td>￥<?php echo $good_info['end_price']; ?></td>
                        </tr>
                    </tbody>

                </table>
            </form>
            <button type="button" onclick="suspendGood(<?php echo $good_info['id']; ?>)" class="layui-btn" >暂停拍品</button>
            <button type="button" onclick="startGood(<?php echo $good_info['id']; ?>)" class="layui-btn" >开始拍品</button>
            <button type="button" onclick="giveUpGood(<?php echo $good_info['id']; ?>)" class="layui-btn" >放弃拍品</button>
        </div>
    </fieldset>
</div>
<script type="text/javascript" src="__ADMIN__/plugins/layui/layui.js"></script>
<script src="__STATIC__/js/jquery.2.1.1.min.js"></script>
<script src="http://www.jq22.com/jquery/jquery-migrate-1.2.1.min.js"></script>
<script src="__STATIC__/js/jquery.jqprint.js"></script>
<script>
        layui.config({
        base: '__ADMIN__/js/'
    }).use(['paging', 'code','icheck','layer'], function() {

    });
        function getdate(date){
            var date = new Date(date*1000);//如果date为10位不需要乘1000
            var Y = date.getFullYear() + '-';
            var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
            var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
            var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
            var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
            var s = (date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds());
            return Y+M+D+h+m+s;
        }

        function stateyes(id) {
            $.post('<?php echo url("goodsState"); ?>', {id: id}, function (data) {
                if (data.status) {
                    if (data.info == '已撤回') {
                        var a = '<button class="layui-btn layui-btn-danger layui-btn-mini">已撤回</button>'
                        $('#zt' + id).html(a);
                        layer.msg(data.info, {icon: 5});
                        return false;
                    } else {
                        var b = '<button class="layui-btn layui-btn-warm layui-btn-mini">已发布</button>'
                        $('#zt' + id).html(b);
                        layer.msg(data.info, {icon: 6});
                        return false;
                    }
                }else{
                    layer.msg(data.msg,{time:1000,icon:2});
                    return false;
                }
            });
            return false;
        }


        function suspendGood(id) {
            if(<?php echo $good_info['is_pause']; ?> == 0){
                alert('该拍品状态已经是暂停');
                return ;
            }
            layer.confirm('你确定要暂停拍品吗？', {icon: 3}, function (index){
                layer.close(index);
                window.location.href = "<?php echo url('suspendGood'); ?>?id=" + id;
            });
        }

        function startGood(id) {
            if(<?php echo $good_info['is_pause']; ?> == 1){
                alert('该拍品状态已经是开始，拍卖进行中');
                return ;
            }
            layer.confirm('你确定要开始该拍品吗？', {icon: 3}, function (index){
                layer.close(index);
                window.location.href = "<?php echo url('startGood'); ?>?id=" + id;
            });
        }

        function giveUpGood(id) {
            if(<?php echo $good_info['is_giveup']; ?> == 1){
                alert('该拍品状态已经被放弃');
                return ;
            }
            layer.confirm('你确定要放弃该拍品吗？', {icon: 3}, function (index){
                layer.close(index);
                window.location.href = "<?php echo url('giveUpGood'); ?>?id=" + id;
            });
        }

</script>