<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:57:"E:\phpstudy\WWW\pcpaimai/app/jinjia\view\index\index.html";i:1512389970;s:57:"E:\phpstudy\WWW\pcpaimai/app/jinjia\view\common\head.html";i:1511854287;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>首页</title>
    <link type="text/css" rel="stylesheet" href="__JINJIA__/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="__JINJIA__/css/style.css">
    <style type="text/css">

    </style>
</head>
<body>
<div id="maincontainer">
    <div class="container-fluid">
        <!-- 顶部包括logo和登录 -->
        <div style="width:100%;">
            <a href="<?php echo url('index'); ?>"><img class="img-logo" src="__JINJIA__/images/logo.jpg"></a><span>价高得网络竞价平台&nbsp;&nbsp;&nbsp;&nbsp;服务热线：028-888888</span>
            <p style="display:inline-block; margin-left:60%;">
                <a href="<?php echo url('User/regist'); ?>" style="display: <?php if($myuser['nickname']) echo 'none'; ?>;" class="text-right"><font color="black">注册</font></a>
                <a href="<?php echo url('User/login'); ?>" style="display: <?php if($myuser['nickname']) echo 'none'; ?>;" class="text-right"><font color="black">登录</font></a>
                <?php if($myuser['nickname']) echo '用户：'.$myuser['nickname']; ?>
            </p>
        </div>
        <div id="mynav">
            <nav class="navbar" style="margin-bottom: 0px;">
                <!-- 导航 -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="<?php if($action=='index') echo 'active'; ?>"><a href="<?php echo url('index'); ?>"><font color="#FFFFFF">网拍首页</font><span class="sr-only"></span></a></li>
                        <li class="<?php if($action=='soon') echo 'active'; ?>"><a href="<?php echo url('soon'); ?>"><font color="#FFFFFF">即将拍卖</font><span class="sr-only"></span></a></li>
                        <li class="<?php if($action=='now') echo 'active'; ?>" ><a href="<?php echo url('now'); ?>"><font color="#FFFFFF">正在拍卖</font><span class="sr-only"></span></a></li>
                        <li class="<?php if($action=='success') echo 'active'; ?>" ><a href="<?php echo url('salesuccess'); ?>"><font color="#FFFFFF">成功拍得</font><span class="sr-only"></span></a></li>
                        <li class="<?php if($action=='auction') echo 'active'; ?>" ><a href="<?php echo url('auction'); ?>"><font color="#FFFFFF">拍卖会</font><span class="sr-only"></span></a></li>
                    </ul>
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" id="navSearch" class="form-control" placeholder="输入文字进行搜索">
                            <div id="search">
                                <a href="javascript:void(0);" onclick="navSearch()">
                                    <font size="3" color="#FFFFFF">搜索</font>
                                </a>
                            </div>
                        </div>
                        <!--  <button type="submit" class="btn btn-default">搜索</button> -->
                    </form>
                </div>
            </nav>
        </div>
        <!-- 轮播 -->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="__JINJIA__/images/timg.jpg" alt="..." style="height: 400px; width:100%;">
                    <div class="carousel-caption">
                    </div>
                </div>
                <div class="item">
                    <img src="__JINJIA__/images/timg.jpg" alt="..." style="height: 400px; width:100%;">
                    <div class="carousel-caption"></div>
                </div>
                <div class="item">
                    <img src="__JINJIA__/images/timg.jpg" alt="..." style="height: 400px; width:100%;">
                    <div class="carousel-caption"></div>
                </div>
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <!-- 下面的拍卖区 -->
        <div>
            <!--正在拍卖-->
            <div style="width:100%">
                <h3 class="pai-tag"><strong>正在拍卖</strong></h3>
                <p class="pai-more"><a href="<?php echo url('now'); ?>"><font color="grey">更多</font></a></p>
            </div>
            <div id="par_list">
                <ul>
                    <?php if(is_array($now_list) || $now_list instanceof \think\Collection || $now_list instanceof \think\Paginator): $i = 0;$__LIST__ = is_array($now_list) ? array_slice($now_list,0,5, true) : $now_list->slice(0,5, true); if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <li>
                        <a href="<?php echo url('Jinpai/index'); ?>?id=<?php echo $vo['id']; ?>&is_auct=<?php echo $vo['is_auct']; ?>"><img src="__PUBLIC__/<?php echo $vo['thumb1']; ?>" style="width:200px; height:150px;"></a>
                        <p class="te-o"><strong><?php echo $vo['title']; ?></strong></p>
                        <p>搜索编号:<?php echo $vo['id']; ?></p>
                        <p class="jg"><font color="grey">当前价格：</font><font><strong>￥<?php echo $vo['maxprice']; ?>元</strong></font></p>
                        <p class="jj"><font color="grey">加价幅度：</font><font><strong>￥<?php echo $vo['marup_price']; ?>元</strong></font></p>
                        <p class="jl">
                            <font color="grey">开始时间：</font><font><strong><?php echo date("m-d h:i",$vo['pmhtime'] ); ?></strong></font><span><?php echo $vo['user_count']; ?>人<br>加价</span>
                        </p>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div style="clear: both;"></div>
            </div>
        </div>
        <br>
        <div>
            <!--即将拍卖-->
            <div style="width:100%">
                <h3 class="pai-tag"><strong>即将拍卖</strong></h3>
                <p class="pai-more"><a href="<?php echo url('soon'); ?>"><font color="grey">更多</font></a></p>
            </div>
            <div id="par_list1">
                <ul>
                    <?php if(is_array($soon_list) || $soon_list instanceof \think\Collection || $soon_list instanceof \think\Paginator): $i = 0;$__LIST__ = is_array($soon_list) ? array_slice($soon_list,0,5, true) : $soon_list->slice(0,5, true); if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <li>
                        <a href="<?php echo url('Jinpai/index'); ?>?id=<?php echo $vo['id']; ?>&is_auct=<?php echo $vo['is_auct']; ?>"><img src="__PUBLIC__/<?php echo $vo['thumb1']; ?>" style="width:200px; height:150px;"></a>
                        <p class="te-o"><strong><?php echo $vo['title']; ?></strong></p>
                        <p>搜索编号:<?php echo $vo['id']; ?></p>
                        <p class="jg"><font color="grey"><?php echo date("h时i分s秒",$vo['pmhtime'] ); ?>后</font></p>
                        <p><font color="red">开始拍卖</font></p>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div style="clear: both;"></div>
            </div>
        </div>
        <br>
        <div>
            <!--拍卖结束-->
            <div style="width:100%">
                <h3 class="pai-tag"><strong>拍卖结束</strong></h3>
                <p class="pai-more"><a href=""><font color="grey">更多</font></a></p>
            </div>
            <div id="par_list2">
                <ul>
                    <?php if(is_array($end_list) || $end_list instanceof \think\Collection || $end_list instanceof \think\Paginator): $i = 0;$__LIST__ = is_array($end_list) ? array_slice($end_list,0,5, true) : $end_list->slice(0,5, true); if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                    <li>
                        <a href="<?php echo url('Jinpai/index'); ?>?id=<?php echo $vo['id']; ?>&is_auct=<?php echo $vo['is_auct']; ?>"><img src="__PUBLIC__/<?php echo $vo['thumb1']; ?>" style="width:200px; height:150px;"></a>
                        <p class="te-o"><strong><?php echo $vo['title']; ?></strong></p>
                        <p>搜索编号:<?php echo $vo['id']; ?></p>
                        <p>状态:
                            <?php
                                    if($vo['is_auct'] ==1){
                                         if($good_info['is_giveup']==1){
                                            echo '被放弃';
                                         }else{
                                             if($good_info['is_pause']==1){
                                                echo '拍卖中';
                                             }else{
                                                //未开始或被暂停（这块儿不好区分）
                                                if($good_info['starttime'])//如果存在开始时间肯定是开始了的被暂停了
                                                    echo '被暂停';
                                                else
                                                    echo '等待开始';
                                             }
                                         }
                                 }else{
                                    echo '拍卖中';
                                 }
                                ?>
                        </p>
                        <p class="jg"><font color="grey">成交价格：</font><font><strong>￥<?php echo $vo['get_price']; ?>元</strong></font></p>
                        <p class="jj"><font color="grey">加价幅度：</font><font><strong>￥<?php echo $vo['marup_price']; ?>元</strong></font></p>
                        <p class="jl"><font color="grey"><?php echo date("Y-m-d",$vo['endtime'] ); ?>结束拍卖</font><span><?php if($vo['is_sure']=='1') echo '流拍'; else echo '已成交'; ?></span></p>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
                <div style="clear: both;"></div>
            </div>
        </div>
    </div>
</div>
</body>

<script src="__JINJIA__/js/jquery.min.js"></script>
<script src="__JINJIA__/js/bootstrap.min.js"></script>
<script src="__JINJIA__/js/style.js"></script>
<script src="__JINJIA__/js/index.js"></script>
<script>
    $(function () {
        webInit.webInit(<?php echo $myservertime; ?>);
        //alert(myconfig.servertime);
    })
</script>
</html>