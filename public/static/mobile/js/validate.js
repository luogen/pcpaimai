/*用户登录*/
function checkLogin(form) {
    if(form.nickname.value=='') {
        alert("请输入用户名!");
        form.nickname.focus();
        return false;
    }
    if(form.password.value==''){
        alert("请输入登录密码!");
        form.password.focus();
        return false;
    }

    return true;
}
/*用户注册*/
function checkRegist(form) {
    if(form.nickname.value=='') {
        alert("请输入用户名!");
        form.nickname.focus();
        return false;
    }
    if(form.password.value==''){
        alert("请输入登录密码!");
        form.password.focus();
        return false;
    }
    if(form.repassword.value==''){
        alert("请输入确认密码!");
        form.repassword.focus();
        return false;
    }else{
        if(form.password.value!=form.repassword.value){
            alert("两次密码不一致，请重新输入!");
            form.password.focus();
            return false;
        }
    }
    if(form.mobile.value==''){
        alert("请输入手机号码!");
        form.mobile.focus();
        return false;
    }
    if(form.code.value==''){
        alert("请输入短信验证码!");
        form.code.focus();
        return false;
    }
    return true;
}
//获取短信验证码
function getPhoneCode(){
    //$(".getcode").attr('disabled',true);设置按钮可以点击
    //$(".getcode").attr('disabled',false);
    $(".getcode").attr('disabled',true);
    var cutSec=60;
    var timer=window.setInterval(function () {
        cutSec=cutSec - 1;
        if(cutSec >0){
            $('.getcode').html(cutSec+'s');
        }else{
            $(".getcode").attr('disabled',false);
            $('.getcode').html('重新发送');
            clearInterval(timer);
            return ;
        }
    },1000);
}