var myConfig={
    good_info:{
        goods_id:0,
        maxprice:0.00,//当前最高价
        startprice:0.00,
        marupprice:0.00,
        endprice:0.00,
        cuttime:0,
        is_end:0,//是否结束
        is_sure:0,//是否流拍
        starttime:0
    },
    user_info:{
        user_id:0,
        phone:0,//
        status:0,//0为未审核，1为通过审核，2为被驳回
        usernumber:0//拍品竞价编号
    },
    servertime:0,//服务器时间
    record_cuttime:{
        begintime:0,//距离开始剩余秒速
        cuttime:0,//限时竞价剩余秒速
    },
    good_bidden_status:0,//拍品是否有出价记录,1代表有0代表没有,如果有的话取的是最新的那条记录
    newest_bidden_info:{//最新的出价记录
        cretime:0,
        price:0.00,
        user_id:0,
        usernumber:0
    },
};
//为页面的拍卖师和竞价记录赋值
var FuZhi={
    //拍卖师的发言
    pmsWords:function (data) {
        var strHtml='';
        var allStrHtml='';
        $.each(data,function(index,item){
            if(index==0){//第一行发言颜色不同
                strHtml=strHtml+'<li class="fayannow">拍卖师：'+item['text']+'<span class="paims-time">'+Tools.UnixToDate(item['cretime'],true)+'</span></li>';
                allStrHtml=allStrHtml+'<tr class="fayannow"><td class="text-left">拍卖师：'+item['text']+'</td><td>'+Tools.UnixToDate(item['cretime'],true)+'</td></tr>';
            }else{
                if(index <=3)
                    strHtml=strHtml+'<li class="fayanlast">拍卖师：'+item['text']+'<span class="paims-time">'+Tools.UnixToDate(item['cretime'],true)+'</span></li>';
                allStrHtml=allStrHtml+'<tr class="fayanlast"><td class="text-left">拍卖师：'+item['text']+'</td><td>'+Tools.UnixToDate(item['cretime'],true)+'</td></tr>';
            }

        });
        $('#pms-words').html(strHtml);
        $('all-pms-con').html(allStrHtml);
    },
    //出价记录
    biddenList:function (data) {
        var strHtml='';//全部竞价记录
        var mystrHtml='';//只有10条记录
        $.each(data,function(index,item){
            if(index==0){//第一行发言颜色不同
                myConfig.good_info.max_price=item['price'];
                var price=item['price']+parseFloat(myConfig.good_info.marup_price);
                //alert(price);
                $("#now_max_price").text('￥'+item['price']+'元');
                $('#pai-price').val(price+'');
                strHtml=strHtml+'<tr style="color:red;"><td>'+item['usernumber']+'</td><td>目前最高价</td><td>'+Tools.UnixToDate(item['cretime'],true)+'</td><td>￥'+item['price']+'元</td></tr>';
                mystrHtml=mystrHtml+'<tr style="color:red;"><td>'+item['usernumber']+'</td><td>目前最高价</td><td>'+Tools.UnixToDate(item['cretime'],true)+'</td><td>￥'+item['price']+'元</td></tr>';
                //有人出价恢复倒计时时间
                myConfig.record_cuttime.cuttime=myConfig.good_info.cuttime;
                //赋值最新的出价记录
                myConfig.newest_bidden_info.price=item['price'];
                myConfig.newest_bidden_info.cretime=item['cretime'];
                myConfig.newest_bidden_info.usernumber=item['usernumber'];
                myConfig.newest_bidden_info.user_id=item['user_id'];
            }else{
                strHtml=strHtml+'<tr style="color:gray;"><td>'+item['usernumber']+'</td><td>已出局</td><td>'+ Tools.UnixToDate(item['cretime'],true)+'</td><td>￥'+item['price']+'元</td></tr>';
                if(index <= 9){
                    mystrHtml=mystrHtml+'<tr style="color:gray;"><td>'+item['usernumber']+'</td><td>已出局</td><td>'+ Tools.UnixToDate(item['cretime'],true)+'</td><td>￥'+item['price']+'元</td></tr>';
                }
            }
        });
        $('#bidden-con').html(mystrHtml);
        $('#all-bidden-con').html(strHtml);
    },
    //设置出价按钮灰色不可以点击
    set_sendPricedisable:function (status,msg) {
        var sendPricebtn=$('#sendPrice');
        if(status){//true
            sendPricebtn.attr('disabled',true);
            sendPricebtn.css('background-color','gray');
            sendPricebtn.text(msg);
        }else{
            sendPricebtn.removeAttr('disabled');
            sendPricebtn.css('background-color','#B70005');
            sendPricebtn.text(msg);
        }

    },
    //设置拍卖资格灰色不可以点击
    set_applydisable:function (status,msg) {
        var applybtn=$('#applybtn');
        if (status){//true
            //关闭申请按钮
            applybtn.attr('disabled',true);
            applybtn.css('background-color','gray');
            applybtn.text(msg);//等待后台审核/被放弃/已经停止
        }else{//显示
            applybtn.removeAttr('disabled');
            applybtn.css('background-color','#B70005');
            applybtn.text(msg);
        }

    },
    //设置出价按钮显示与否
    set_sendprice_show:function (status) {
        var sendPricebtn=$('#sendPrice');
        if(status){//true
            sendPricebtn.show();
        }else{//false
            sendPricebtn.hide();
        }
    },
    //设置申请资格按钮显示与否
    set_apply_show:function (status) {
        var applybtn=$('#applybtn');
        if(status){//true
            applybtn.show();
        }else{//false
            applybtn.hide();
        }
    },
    //设置距离结束倒计时字符串
    set_time_str:function (str_type,str_time) {
        $('#time_type').text(str_type+'：');//设置倒计时的阶段（距离拍卖/自由竞价/限时竞价）
        $('#time_str').text(str_time+'');//设置时间
    }
};
//出价
var biddenPrice={
    //加减价按钮
    addPrice:function () {
        var cuval=parseFloat($('#pai-price').val());
        $('#pai-price').val(cuval+myConfig.good_info.marup_price);
    },
    cutPrice:function () {
        var val=$('#pai-price').val();
        var now_price=parseFloat(val - myConfig.good_info.marup_price);
        if(myConfig.good_bidden_status == 0){//还没有出价记录
            if(now_price > myConfig.good_info.max_price || now_price==myConfig.good_info.start_price)
                $('#pai-price').val(now_price);
            else
                alert('不能低于当前最高价');
        }else{
            if(now_price > myConfig.good_info.max_price)
                $('#pai-price').val(now_price);
            else
                alert('不能低于当前最高价');
        }
    },
    chuJia:function () {
        if(myConfig.good_info.is_end == 1 ){
            alert('错误操作');
            return ;
        }
        if(myConfig.user_info.status != 1){
            alert('资格审核中，请等待');
            return ;
        }
        var val=$('#pai-price').val();

        if(myConfig.good_bidden_status == 0){
            //还没有出价记录
            if(val < myConfig.good_info.start_price){
                alert('第一次出价必须大于或等于起拍价');
                return ;
            }
        }else{
            if(val < parseFloat(myConfig.good_info.max_price + myConfig.good_info.marup_price)){
                alert('手动出价必须大于或等于最高价加加价幅度');
                return ;
            }
        }
        //alert(val);
        if(window.confirm("出价"+val+"元")){
            //确定
            $.post(
                '/index.php/disport/Jinpai/dangoodBidden',
                {user_id:myConfig.user_info.user_id,goods_id:myConfig.good_info.goods_id,price:val,usernumber:myConfig.user_info.usernumber,tag:myConfig.current_stage},
                function (data){
                    data=JSON.parse(data);
                    if(data.status=='ok') {
                        //出价成功 wait106
                        alert('出价成功');
                    }else if(data.status=='error'){
                        alert("出价错误，请重试");
                    }else if(data.status=='failed'){
                        alert('出价无效');
                    }
                }
            );
        }else{
            //取消
        }
    },
};
/**
 * 拍品页面拍品图片切换
 * @type {{setImgUrl: imgList.setImgUrl}}
 */
var imgList={
    setImgUrl : function (obj) {
        $('#conleft-imglist ul li').removeClass('imgaction');
        var url=$(obj).attr('src');
        $(obj).parent().addClass('imgaction');
        $('#main-img').attr('src',url);
    }
};
/**
 * 申请拍卖资格和向后台发送拍品正常结束
 * @type {{checkApply: apply.checkApply}}
 */
var apply={
    //申请拍卖资格
    checkApply:function (form) {
        var myapply=new Array();
        myapply['username']=form.username.value;
        if(form.username.value=='') {
            alert("请输入真实姓名!");
            form.username.focus();
            return false;
        }
        myapply['phone']=form.phone.value;
        if(myapply['phone']==''){
            alert("请输入手机号!");
            form.phone.focus();
            return false;
        }else if(Tools.isPhoneNo(myapply['phone'])==false){
            alert("手机号格式不正确!");
            form.phone.focus();
            return false;
        }
        $('#applyclose').trigger("click");//关闭模态框
        //关闭申请按钮
        var applybtn=$('#applybtn');
        applybtn.attr('disabled',true);
        applybtn.text('等待后台审核');
        //请求服务器
        $.post(
            '/index.php/disport/Jinpai/danapply',
            {username:myapply['username'],phone:myapply['phone'],user_id:myConfig.user_info.user_id,goods_id:myConfig.good_info.goods_id},
            function (data) {
                data=JSON.parse(data);
                if(data.status=='ok'){
                    alert('申请成功请等待后台审核');
                    myInterval.applyInterval();
                }else{
                    alert('申请资格失败，请刷新重试！');
                }
            }
        );
    },
    //拍品结束
    goodFinish:function () {
        $.post(
            '/index.php/disport/Jinpai/dangoodFinish',
            {goods_id:myConfig.good_info.goods_id,max_price:myConfig.good_info.max_price,user_id:myConfig.newest_bidden_info.user_id},
            function (data) {
                data=JSON.parse(data);
                if(data.status=='ok'){
                    //alert('拍品结束');//弹出是流拍还是成交(由推送来给出)
                }else{

                }
            }
        );
    }
};
/**
 * 初始化标签页
 * @type {{init: myTab.init}}
 */
var myTab={
    init:function () {
        $('#myTab a:eq(0)').tab('show');//初始化底部标签页
        $('#myTab a').click(function(e) {
            $('#myTab a').removeClass('rec-active');
            $(this).addClass('rec-active');
        });
    }
};
var myInterval={
    //拍卖会开始倒计时->自由竞价->限时竞价
    startInterval:function () {
        var str=Tools.getDate(myConfig.record_cuttime.begintime);//获取当前剩余的时间字符串
        myConfig.record_cuttime.begintime=myConfig.record_cuttime.begintime-1;//减去一秒
        //设置到页面
        FuZhi.set_time_str('距离开始',str);
        if( myConfig.record_cuttime.begintime<=0){//拍卖会倒计时结束
            //开启竞价倒计时或者返回结束标志
            myConfig.record_cuttime.cuttime=myConfig.good_info.cuttime;//设置自由竞价倒计时时间
            if(myConfig.user_info.status != 1){
                //审核未通过或者未申请

            }else{
                FuZhi.set_sendprice_show(true);
                FuZhi.set_apply_show(false);
            }
            Tools.recoverBtn();

            myInterval.cutInterval();//开启自由竞价
        }else{
            setTimeout("myInterval.startInterval();", 1000);//一秒后再执行
        }
    },
    //拍品限时竞价倒计时
    cutInterval:function () {
        var str=Tools.getDate(myConfig.record_cuttime.cuttime);//获取当前剩余的时间字符串
        myConfig.record_cuttime.cuttime=myConfig.record_cuttime.cuttime - 1;//减去一秒
        //设置到页面
        FuZhi.set_time_str('限时竞价',str);
        //alert('限时');
        if( myConfig.record_cuttime.cuttime<=-2){//拍卖会倒计时结束
            //限时竞价结束（向后台发送结束标志）
            apply.goodFinish();
            //设置页面
            Tools.setEnd();
            return ;
        }else{
            setTimeout("myInterval.cutInterval();", 1000);//一秒后再执行
        }
    },
    //用户申请拍卖资格后的全局定时器
    applyInterval:function () {
        var my_apply_status=window.setInterval(function () {
            $.post(
                '/index.php/disport/Jinpai/danapplyStatus',
                {goods_id:myConfig.good_info.goods_id,user_id:myConfig.user_info.user_id},
                function (data) {
                    data=JSON.parse(data);
                    if(data.status == 'ok'){
                        window.location.reload();//刷新页面
                    }else if(data.status == 'bohui'){
                        alert('拍卖资格被驳回，请重新申请');
                        Tools.recoverBtn();
                        FuZhi.set_sendprice_show(false);
                        FuZhi.set_apply_show(true);
                        Tools.myClearInterval(my_apply_status);
                    }else{
                        //alert(data.status);
                    }
                },
                'json'
            );
        },3000);
    }
};
//常用工具
var Tools={
    /**
     * 时间戳转换日期
     * @param <int> unixTime  待时间戳(秒)
     * @param <bool> isFull  返回完整时间(Y-m-d 或者 Y-m-d H:i:s)
     */
    UnixToDate: function(unixTime, isFull) {
        var time = new Date(unixTime * 1000);
        var ymdhis = "";
        //ymdhis += time.getUTCFullYear() + "-";
        if((time.getUTCMonth()+1)<=9)
            ymdhis += "0"+ (time.getUTCMonth()+1) + "-";
        else
            ymdhis += (time.getUTCMonth()+1) + "-";
        if(time.getUTCDate()<=9)
            ymdhis += "0"+time.getUTCDate();
        else
            ymdhis += time.getUTCDate();
        if (isFull === true)
        {
            if( time.getUTCHours()<=9)
                ymdhis += " " +"0"+ time.getUTCHours() + ":";
            else
                ymdhis += " " + time.getUTCHours() + ":";
            if(time.getUTCMinutes()<=9)
                ymdhis += "0"+time.getUTCMinutes() + ":";
            else
                ymdhis += time.getUTCMinutes() + ":";
            if(time.getUTCSeconds()<=9)
                ymdhis +="0"+time.getUTCSeconds();
            else
                ymdhis += time.getUTCSeconds();
        }
        return ymdhis;
    },
    /**
     * 也是根据时间戳返回日期格式
     * @param intDiff
     * @returns {string}
     */
    getDate : function (intDiff) {//秒数
        var day = 0,
            hour = 0,
            minute = 0,
            second = 0; //时间默认值
        if (intDiff > 0) {
            day = Math.floor(intDiff / (60 * 60 * 24));
            hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
            minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
            second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
        }
        if (hour <= 9) hour = '0' + hour;
        if (minute <= 9) minute = '0' + minute;
        if (second <= 9) second = '0' + second;
        var timestr=day+"天"+hour+"时"+minute+"分"+second+"秒";
        return timestr;
    },
    /**
     * 清除定时器
     * @param interval
     */
    myClearInterval : function (interval) {//清除定时器
        clearInterval(interval);
    },
    /**
     * 验证手机号 返回布尔值
     * @param phone
     * @returns {boolean}
     */
    isPhoneNo : function (phone) {
        var pattern = /^1[34578]\d{9}$/;
        return pattern.test(phone);
    },
    /**
     * 设置结束
     */
    setEnd:function () {
        alert('该拍品已经结束');
        myConfig.good_info.is_end=1;//拍品结束标志
        FuZhi.set_applydisable(true,'拍品已结束');//设置出价按钮灰色不可以点击
        FuZhi.set_sendPricedisable(true,'拍品已结束');//设置申请资格按钮灰色不可以点击
    },
    setWait:function () {
        alert('该拍品未开始，请等待！');
        FuZhi.set_applydisable(true,'请等待拍品开始');
        FuZhi.set_sendPricedisable(true,'请等待拍品开始');//设置出价按钮灰色不可以点击
        //FuZhi.set_applydisable();//被停止可以申请拍卖资格
    },
    /**
     * 恢复按钮
     */
    recoverBtn:function () {
        FuZhi.set_applydisable(false,'申请资格');//设置出价按钮灰色不可以点击
        //alert('恢复');
        FuZhi.set_sendPricedisable(false,'出价');//设置申请资格按钮灰色不可以点击
    },

};
//主要用于判断
var judgeTools={
    //判断拍品拍卖是否开始
    judge_start:function () {
        var tag= myConfig.good_info.starttime - myConfig.servertime;
        if(tag > 0){//未开始
            myConfig.record_cuttime.begintime=tag;//设置拍卖会倒计时剩余的秒数
            return false;
        }
        else{//已经开始
            //是否有出价
            if(myConfig.good_bidden_status ==1 ){
                //有出价
                var time=(myConfig.newest_bidden_info.cretime+myConfig.good_info.cuttime)-myConfig.servertime;
                if(time <= 0){
                    //拍品已经结束

                }else{
                    //设置剩余的倒计时秒数
                    myConfig.record_cuttime.cuttime=time;
                }
            }else{
                //无出价
                var time=(myConfig.good_info.starttime + myConfig.good_info.cuttime)- myConfig.servertime;
                if(time <= 0){
                    //拍品已经结束

                }else{
                    //设置剩余的倒计时秒数
                    myConfig.record_cuttime.cuttime=time;
                }
            }
            return true;
        }
    },
    //判断拍品是否结束
    judge_end:function () {
        if(myConfig.good_info.is_end == 1){
            return true;
        }else{
            return false;
        }
    },
    //
};
/**
 * websocket工具
 * @type {{init: WsTools.init}}
 */
var WsTools={
    init:function () {
        /**
         * 与GatewayWorker建立websocket连接，域名和端口改为你实际的域名端口，
         * 其中端口为Gateway端口，即start_gateway.php指定的端口。
         * start_gateway.php 中需要指定websocket协议，像这样
         * $gateway = new Gateway(websocket://0.0.0.0:7272);
         */
        ws = new WebSocket("ws:paimai1.9rui.cn:7272");
        // 服务端主动推送消息时会触发这里的onmessage
        ws.onmessage = function(e){
            // json数据转换成js对象
            var data = eval("("+e.data+")");
            var type = data.type || '';
            switch(type){
                // Events.php中返回的init类型的消息，将client_id发给后台进行uid绑定
                case 'init':
                    WsTools.bindClientid(data.client_id);
                    break ;
                case 'words':
                    WsTools.setPmsWords(data.info);
                    break ;
                case 'bidden':
                    myConfig.servertime=data.servertime;
                    WsTools.setBiddenList(data.info);
                    break ;
                case 'end'://结束
                    WsTools.setGoodEnd(data.info);
                default :
                //alert(e.data);
            }
        };
    },
    /**
     * type为init
     * 连接成功时，将客户端id发送到后台绑定
     */
    bindClientid:function (client_id) {
        $.post(
            '/index.php/disport/Jinpai/danbindClientId',
            {goods_id:myConfig.good_info.goods_id,client_id:client_id},
            function (data) {
                data=JSON.parse(data);
                if(data.status == 'ok'){
                    //alert('绑定成功');
                }else{
                    alert('出错啦，需要退出，重新登录');
                }
            },
            'json'
        );
    },
    /**
     * type为biiden
     * 服务器推送的竞价的信息
     */
    setBiddenList:function (data) {
        if(data != null){
            myConfig.good_bidden_status=1;
        }else{
            myConfig.good_bidden_status=0;
        }
        FuZhi.biddenList(data);
    },
    /**
     *type为words
     * 服务器推送的拍卖师发言的信息
     */
    setPmsWords:function (data) {
        //alert(data[1].text);
        FuZhi.pmsWords(data);
    },
    /**
     * type为end
     * 拍品已经结束
     */
    setGoodEnd:function (data) {
        alert(data.msg);
        window.location.reload();//刷新
    },
};

var startInit={
    //逻辑启动
    init:function () {
        if(judgeTools.judge_end()){
            //已经结束
            Tools.setEnd();
        }else{
            //未结束
            //alert('未结束');
            if(judgeTools.judge_start()){
                //已经开始
                //alert('已经开始');
                FuZhi.set_sendprice_show(true);
                FuZhi.set_apply_show(false);
                myInterval.cutInterval();
            }else{
                //未开始（距离开始倒计时）
               // alert('未开始');
                FuZhi.set_sendprice_show(false);
                FuZhi.set_apply_show(true);
                Tools.setWait();
                myInterval.startInterval();
            }
            //未申请拍卖资格的就一定需要申请拍卖资格
            if(myConfig.user_info.status===0){//等待审核
                alert('拍卖资格等待审核中！');
                FuZhi.set_sendprice_show(false);
                FuZhi.set_apply_show(true);
                FuZhi.set_applydisable(true,'等待审核');
                myInterval.applyInterval();
            }else if(myConfig.user_info.status==1){//通过

            }else if(myConfig.user_info.status==2){//被驳回
                alert('申请被驳回，请重新申请！');
                Tools.recoverBtn();
                FuZhi.set_sendprice_show(false);
                FuZhi.set_apply_show(true);
            }else if(myConfig.user_info.status==null){//还未申请
                // alert('未申请拍卖资格');
                FuZhi.set_sendprice_show(false);
                FuZhi.set_apply_show(true);
                Tools.recoverBtn();
            }
        }
    },
    //赋值
    initInfo:function (good_info,user_info,newest_bidden_info,other_info) {
        console.log(good_info);
        console.log(user_info);
        console.log(newest_bidden_info);
        startInit.initGoodInfo(good_info);
        startInit.initUserInfo(user_info);
        if(newest_bidden_info){//有出价信息
            startInit.initNewestBiddenInfo(newest_bidden_info);
            myConfig.good_bidden_status=1;
        }
        myConfig.servertime=other_info.servertime;
    },
    //拍品基本信息赋值 data为json字符串
    initGoodInfo:function (data) {
        myConfig.good_info.goods_id= data.id;
        myConfig.good_info.max_price=data.max_price;
        myConfig.good_info.start_price=data.start_price;
        myConfig.good_info.marup_price=data.marup_price;
        myConfig.good_info.end_price=data.end_price;
        myConfig.good_info.cuttime=data.cuttime;
        myConfig.good_info.is_end=data.is_end;
        myConfig.good_info.is_sure=data.is_sure;
        myConfig.good_info.starttime=data.starttime;
    },
    //用户信息赋值
    initUserInfo:function (data) {
        myConfig.user_info.user_id=data.user_id;
        myConfig.user_info.usernumber=data.usernumber;
        myConfig.user_info.phone=data.phone;
        myConfig.user_info.status=data.status;
        //alert(data.usernumber);
    },
    //最新竞价记录赋值
    initNewestBiddenInfo:function (data) {
        myConfig.newest_bidden_info.cretime=data.cretime;
        myConfig.newest_bidden_info.price=data.price;
        myConfig.newest_bidden_info.user_id=data.user_id;
        myConfig.newest_bidden_info.usernumber=data.usernumber;
    }

};
$(function () {
    myTab.init();//初始化标签页
    //alert((7-14)+5);
    WsTools.init();
});
