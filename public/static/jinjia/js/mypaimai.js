/**
 * Created by luobiao on 2017/11/14.
 */
var myConfig={
    good_info:{
        goods_id:0,
        max_price:0.00,//当前最高价
        start_price:0.00,
        marup_price:0.00,
        end_price:0.00,
        cuttime:0,
        freetime:0,
        is_end:0,//是否结束
        auct_id:0,//拍卖会id
        is_giveup:0,//是否放弃，1为放弃
        is_pause:0,//暂停或开始//0表示暂停1为开始
        is_sure:0,//是否流拍
        sort:0,//拍品拍卖会内部排序
        starttime:0
    },
    user_info:{
        user_id:0,
        phone:0,//
        status:0,//0为未审核，1为通过审核，2为被驳回
        usernumber:0//拍品竞价编号
    },
    auct_info:{
        starttime:0,//拍卖会的开始时间
        goods_id:0,//拍卖会当前id
    },
    servertime:0,//服务器时间
    current_stage:0,//当前处于哪一个阶段（0拍卖会倒计时/1自由竞价倒计时/2限时竞价倒计时）
    record_cuttime:{
        freetime:0,//自由竞价剩余秒速
        cuttime:0,//限时竞价剩余秒速
        pmhtime:0//拍卖会剩余的倒计时秒数
    },
    good_status_record:0,//拍品是否有状态记录,1代表有0代表没有,如果有的话取的是最新的那条记录
    /*拍品状态的唯一标识（当后台暂停拍品时候，推送一个字符串代表当前这次暂停，收到推送的页面都会
     向后台发送一个状态码，数据库中只存一条。该字符串用于排除之后的请求）后台存的时候，先判断是
     否有该标识的记录再存*/
    good_status_unique:'',//拍品状态记录唯一标识
    good_status_info:{
        cretime:0,
        oktime:0,
        tag:0,//1代表是自由竞价 2代表是限时竞价
        cuttime:0,
        freetime:0
    },
    good_bidden_status:0,//拍品是否有出价记录,1代表有0代表没有,如果有的话取的是最新的那条记录
    newest_bidden_info:{//最新的出价记录
        tag:1,//1为自由竞价2为限时竞价
        cretime:0,
        price:0.00,
        user_id:0,
        usernumber:0
    },

};
//为页面的拍卖师和竞价记录赋值
var FuZhi={
    //拍卖师的发言
    pmsWords:function (data) {
        var strHtml='';
        var allStrHtml='';
        $.each(data,function(index,item){
            if(index==0){//第一行发言颜色不同
                strHtml=strHtml+'<li class="fayannow">拍卖师：'+item['text']+'<span class="paims-time">'+Tools.UnixToDate(item['cretime'],true)+'</span></li>';
                allStrHtml=allStrHtml+'<tr class="fayannow"><td class="text-left">拍卖师：'+item['text']+'</td><td>'+Tools.UnixToDate(item['cretime'],true)+'</td></tr>';
            }else{
                if(index <=3)
                    strHtml=strHtml+'<li class="fayanlast">拍卖师：'+item['text']+'<span class="paims-time">'+Tools.UnixToDate(item['cretime'],true)+'</span></li>';
                allStrHtml=allStrHtml+'<tr class="fayanlast"><td class="text-left">拍卖师：'+item['text']+'</td><td>'+Tools.UnixToDate(item['cretime'],true)+'</td></tr>';
            }

        });
        $('#pms-words').html(strHtml);
        $('all-pms-con').html(allStrHtml);
    },
    //出价记录
    biddenList:function (data) {
        var strHtml='';
        $.each(data,function(index,item){
            if(index==0){//第一行发言颜色不同
                myConfig.good_info.max_price=item['price'];
                var price=item['price']+parseFloat(myConfig.good_info.marup_price);
                //alert(price);
                $("#now_max_price").text('￥'+item['price']+'元');
                $('#pai-price').val(price+'');
                strHtml=strHtml+'<tr id="now-highest"><td>'+item['usernumber']+'</td><td>目前最高价</td><td>'+Tools.UnixToDate(item['cretime'])+'</td><td>￥'+item['price']+'元</td></tr>';
                //判断是自由竞价阶段还是限时阶段
                if(myConfig.current_stage==1){//自由竞价阶段
                    var mytag=judgeTools.judge_free();
                }
                else if(myConfig.current_stage==2)
                    myConfig.record_cuttime.cuttime=myConfig.good_info.cuttime;
                //赋值最新的出价记录
                myConfig.newest_bidden_info.price=item['price'];
                myConfig.newest_bidden_info.tag=item['tag']
                myConfig.newest_bidden_info.cretime=item['cretime'];
                myConfig.newest_bidden_info.usernumber=item['usernumber'];
                myConfig.newest_bidden_info.user_id=item['user_id'];
            }else{
                strHtml=strHtml+'<tr id=""><td>'+item['usernumber']+'</td><td>已出局</td><td>'+ Tools.UnixToDate(item['cretime'])+'</td><td>￥'+item['price']+'元</td></tr>';
            }
        });
        $('#bidden-con').html(strHtml);
    },
    //设置出价按钮灰色不可以点击
    set_sendPricedisable:function (status,msg) {
        var sendPricebtn=$('#sendPrice');
        if(status){//true
            sendPricebtn.attr('disabled',true);
            sendPricebtn.css('background-color','gray');
            sendPricebtn.text(msg);
        }else{
            sendPricebtn.removeAttr('disabled');
            sendPricebtn.css('background-color','#B70005');
            sendPricebtn.text(msg);
        }

    },
    //设置拍卖资格灰色不可以点击
    set_applydisable:function (status,msg) {
        var applybtn=$('#applybtn');
        if (status){//true
            //关闭申请按钮
            applybtn.attr('disabled',true);
            applybtn.css('background-color','gray');
            applybtn.text(msg);//等待后台审核/被放弃/已经停止
        }else{//显示
            applybtn.removeAttr('disabled');
            applybtn.css('background-color','#B70005');
            applybtn.text(msg);
        }

    },
    //设置出价按钮显示与否
    set_sendprice_show:function (status) {
        var sendPricebtn=$('#sendPrice');
        if(status){//true
            sendPricebtn.show();
        }else{//false
            sendPricebtn.hide();
        }
    },
    //设置申请资格按钮显示与否
    set_apply_show:function (status) {
        var applybtn=$('#applybtn');
        if(status){//true
            applybtn.show();
        }else{//false
            applybtn.hide();
        }
    },
    //设置距离结束倒计时字符串
    set_time_str:function (str_type,str_time) {
        $('#time_type').text(str_type+'：');//设置倒计时的阶段（距离拍卖/自由竞价/限时竞价）
        $('#time_str').text(str_time);//设置时间
    }
};
//出价
var biddenPrice={
    //加减价按钮
    addPrice:function () {
        var cuval=parseFloat($('#pai-price').val());
        $('#pai-price').val(cuval+myConfig.good_info.marup_price);
    },
    cutPrice:function () {
        var val=$('#pai-price').val();
        var now_price=parseFloat(val - myConfig.good_info.marup_price);
        if(myConfig.good_bidden_status == 0){//还没有出价记录
            if(now_price > myConfig.good_info.max_price || now_price==myConfig.good_info.start_price)
                $('#pai-price').val(now_price);
            else
                alert('不能低于当前最高价');
        }else{
            if(now_price > myConfig.good_info.max_price)
                $('#pai-price').val(now_price);
            else
                alert('不能低于当前最高价');
        }

    },
    chuJia:function () {
        if(myConfig.user_info.status != 1){
            alert('资格审核中，请等待');
            return ;
        }
        var val=$('#pai-price').val();
        if(myConfig.good_bidden_status == 0){
            //还没有出价记录
            if(val < myConfig.good_info.start_price){
                alert('第一次出价必须大于或等于起拍价');
                return ;
            }
        }else{
            if(val < parseFloat(myConfig.good_info.max_price + myConfig.good_info.marup_price)){
                alert('手动出价必须大于或等于最高价加加价幅度');
                return ;
            }
        }
        //alert(val);
        if(window.confirm("出价"+val+"元")){
            //确定
            $.post(
                '/index.php/jinjia/Jinpai/goodBidden',
                {user_id:myConfig.user_info.user_id,goods_id:myConfig.good_info.goods_id,price:val,usernumber:myConfig.user_info.usernumber,tag:myConfig.current_stage},
                function (data){
                    data=JSON.parse(data);
                    if(data.status=='ok') {
                        //出价成功 wait106
                        alert('出价成功');
                    }else if(data.status=='error'){
                        alert("出价错误，请重试");
                    }else if(data.status=='failed'){
                        alert('出价无效');
                    }
                }
            );
        }else{
            //取消
        }

    },
};
/**
 * 拍品页面拍品图片切换
 * @type {{setImgUrl: imgList.setImgUrl}}
 */
var imgList={
    setImgUrl : function (obj) {
        $('#conleft-imglist ul li').removeClass('imgaction');
        var url=$(obj).attr('src');
        $(obj).parent().addClass('imgaction');
        $('#main-img').attr('src',url);
    }
};
/**
 * 申请拍卖资格和向后台发送拍品正常结束
 * @type {{checkApply: apply.checkApply}}
 */
var apply={
    //申请拍卖资格
    checkApply:function (form) {
        var myapply=new Array();
        myapply['username']=form.username.value;
        if(form.username.value=='') {
            alert("请输入真实姓名!");
            form.username.focus();
            return false;
        }
        myapply['phone']=form.phone.value;
        if(myapply['phone']==''){
            alert("请输入手机号!");
            form.phone.focus();
            return false;
        }else if(Tools.isPhoneNo(myapply['phone'])==false){
            alert("手机号格式不正确!");
            form.phone.focus();
            return false;
        }
        $('#applyclose').trigger("click");//关闭模态框
        //关闭申请按钮
        var applybtn=$('#applybtn');
        applybtn.attr('disabled',true);
        applybtn.text('等待后台审核');
        //请求服务器
        $.post(
            '/index.php/jinjia/Jinpai/apply',
            {username:myapply['username'],phone:myapply['phone'],user_id:myConfig.user_info.user_id,goods_id:myConfig.good_info.goods_id},
            function (data) {
                data=JSON.parse(data);
                if(data.status=='ok'){
                    alert('申请成功请等待后台审核');
                    myInterval.applyInterval();
                }else{
                    alert('申请资格失败，请刷新重试！');
                }
            }
        );
    },
    //拍品结束
    goodFinish:function () {
        $.post(
            '/index.php/jinjia/Jinpai/goodFinish',
            {goods_id:myConfig.good_info.goods_id,max_price:myConfig.good_info.max_price,user_id:myConfig.newest_bidden_info.user_id},
            function (data) {
                data=JSON.parse(data);
                if(data.status=='ok'){
                    //alert('拍品结束');//弹出是流拍还是成交(由推送来给出)
                }else{

                }
            }
        );
    },
    //记录拍品状态 here后台
    goodStatus:function () {
        var sytime=0;//剩余的时间
        if(myConfig.current_stage==1)
            sytime=myConfig.record_cuttime.freetime;
        else if(myConfig.current_stage==2)
            sytime=myConfig.record_cuttime.cuttime;
        $.post(
            '/index.php/jinjia/Jinpai/goodStatus',
            {goods_id:myConfig.good_info.goods_id,time:sytime,tag:myConfig.current_stage,unique_str:myConfig.good_status_unique},
            function (data) {
                data=JSON.parse(data);
                if(data.status=='ok'){
                    window.location.reload();
                }else{

                }
            }
        );
    }
};
/**
 * 初始化标签页
 * @type {{init: myTab.init}}
 */
var myTab={
    init:function () {
        $('#myTab a:eq(0)').tab('show');//初始化底部标签页
        $('#myTab a').click(function(e) {
            $('#myTab a').removeClass('rec-active');
            $(this).addClass('rec-active');
        });
    }
};

/*定时器优化方案
 var timeout = false; //启动及关闭按钮
 function time()
 {
 if(timeout) return;
 Method();
 setTimeout(time,100); //time是指本身,延时递归调用自己,100为间隔调用时间,单位毫秒
 }*/
var myInterval={
    //拍卖会开始倒计时->自由竞价->限时竞价
    startInterval:function () {
        var str=Tools.getDate(myConfig.record_cuttime.pmhtime);//获取当前剩余的时间字符串
        myConfig.record_cuttime.pmhtime=myConfig.record_cuttime.pmhtime-1;//减去一秒
        //如果各方面原有未开始前就放弃拍品
        if(judgeTools.judge_giveup()){
            Tools.setGiveup();
            return ;
        }
        //设置到页面
        FuZhi.set_time_str('距离拍卖',str);
        if( myConfig.record_cuttime.pmhtime<=0){//拍卖会倒计时结束
            //开启自由竞价倒计时或者返回结束标志
            myConfig.current_stage=1;//自由竞价倒计时
            myConfig.record_cuttime.freetime=myConfig.good_info.freetime;//设置自由竞价倒计时时间
            myInterval.freeInterval();//开启自由竞价
        }else{
            setTimeout("myInterval.startInterval();", 1000);//一秒后再执行
        }
    },
    //拍品自由竞价倒计时
    freeInterval:function () {
        // alert(myConfig.record_cuttime.freetime);
        var str=Tools.getDate(myConfig.record_cuttime.freetime);//获取当前剩余的时间字符串
        myConfig.record_cuttime.freetime=myConfig.record_cuttime.freetime - 1;//减去一秒
        //设置到页面
        //alert(str);
        FuZhi.set_time_str('自由竞价',str);
        //如果拍品被停止
        if(judgeTools.judge_stop()){
            apply.goodStatus();//记录暂停时候的状态
            Tools.setWait();
            return ;
        }
        if( myConfig.record_cuttime.freetime<=0){//拍卖会倒计时结束
            //开启自由竞价倒计时或者返回结束标志
            myConfig.current_stage=2;//自由竞价倒计时
            myConfig.record_cuttime.cuttime=myConfig.good_info.cuttime;//设置限时竞价倒计时时间
            myInterval.cutInterval();//开启自由竞价
        }else{
            setTimeout("myInterval.freeInterval();", 1000);//一秒后再执行
        }
    },
    //拍品限时竞价倒计时
    cutInterval:function () {
        var str=Tools.getDate(myConfig.record_cuttime.cuttime);//获取当前剩余的时间字符串
        myConfig.record_cuttime.cuttime=myConfig.record_cuttime.cuttime - 1;//减去一秒
        //设置到页面
        FuZhi.set_time_str('限时竞价',str);
        //alert('限时');
        //如果拍品被停止
        if(judgeTools.judge_stop()){
            Tools.setStop();
        }else{
            if( myConfig.record_cuttime.cuttime<=-2){//拍卖会倒计时结束
                //限时竞价结束（向后台发送结束标志）
                apply.goodFinish();
                //设置页面
                Tools.setEnd();
                return ;
            }else{
                setTimeout("myInterval.cutInterval();", 1000);//一秒后再执行
            }
        }
    },
    //用户申请拍卖资格后的全局定时器
    applyInterval:function () {
        var my_apply_status=window.setInterval(function () {
            $.post(
                '/index.php/jinjia/Jinpai/applyStatus',
                {goods_id:myConfig.good_info.goods_id,user_id:myConfig.user_info.user_id},
                function (data) {
                    data=JSON.parse(data);
                    if(data.status == 'ok'){
                        window.location.reload();//刷新页面
                    }else if(data.status == 'bohui'){
                        alert('拍卖资格被驳回，请重新申请');
                        Tools.recoverBtn();
                        FuZhi.set_sendprice_show(false);
                        FuZhi.set_apply_show(true);
                        Tools.myClearInterval(my_apply_status);
                    }else{
                        //alert(data.status);
                    }
                },
                'json'
            );
        },3000);
    }
};
/**
 * websocket工具
 * @type {{init: WsTools.init}}
 */
var WsTools={
    init:function () {
        /**
         * 与GatewayWorker建立websocket连接，域名和端口改为你实际的域名端口，
         * 其中端口为Gateway端口，即start_gateway.php指定的端口。
         * start_gateway.php 中需要指定websocket协议，像这样
         * $gateway = new Gateway(websocket://0.0.0.0:7272);
         */
        ws = new WebSocket("ws:paimai1.9rui.cn:7272");
        // 服务端主动推送消息时会触发这里的onmessage
        ws.onmessage = function(e){
            // json数据转换成js对象
            var data = eval("("+e.data+")");
            var type = data.type || '';
            switch(type){
                // Events.php中返回的init类型的消息，将client_id发给后台进行uid绑定
                case 'init':
                    WsTools.bindClientid(data.client_id);
                    break ;
                case 'upd':
                    WsTools.updGood(data.info);
                    break ;
                case 'words':
                    WsTools.setPmsWords(data.info);
                    break ;
                case 'bidden':
                    myConfig.servertime=data.servertime;
                    WsTools.setBiddenList(data.info);
                    break ;
                case 'stop':
                    WsTools.setGoodStop(data.info);
                    break ;
                case 'start':
                    WsTools.setGoodStart(data.info);
                    break ;
                case 'end':
                    WsTools.setGoodEnd(data.info);
                    break ;
                case 'giveup':
                    WsTools.setGoodGiveup();
                    break ;

                default :
                //alert(e.data);
            }
        };
    },
    /**
     * type为init
     * 连接成功时，将客户端id发送到后台绑定
     */
    bindClientid:function (client_id) {
        $.post(
            '/index.php/jinjia/Jinpai/bindClientId',
            {goods_id:myConfig.good_info.goods_id,client_id:client_id},
            function (data) {
                data=JSON.parse(data);
                if(data.status == 'ok'){
                    //alert('绑定成功');
                }else{
                    alert('出错啦，需要退出，重新登录');
                }
            },
            'json'
        );
    },
    /**
     *type为upd
     * 服务器推送的拍品的修改后的信息
     */
    updGood:function (data) {
        myConfig.good_info.start_price=data.start_price;
        myConfig.good_info.marup_price=data.marup_price;
        myConfig.good_info.end_price=data.end_price;
        $('#my_marup_price').text('加价幅度:'+data.marup_price+'元');
    },
    /**
     *type为words
     * 服务器推送的拍卖师发言的信息
     */
    setPmsWords:function (data) {
        //alert(data[1].text);
        FuZhi.pmsWords(data);
    },
    /**
     * type为biiden
     * 服务器推送的竞价的信息
     */
    setBiddenList:function (data) {
        if(data != null){
            myConfig.good_bidden_status=1;
        }else{
            myConfig.good_bidden_status=0;
        }
        FuZhi.biddenList(data);
    },
    /**
     * type为stop
     * 拍卖师暂停拍品的拍卖
     */
    setGoodStop:function (data) {
        myConfig.good_info.is_pause=data.is_pause;
        myConfig.good_status_unique=data.unique_str;
        //把当前状态传到服务器端
        if(myConfig.current_stage != 0){//不是拍卖会倒计时时间段
            apply.goodStatus();
        }
    },
    /**
     * type为start
     * 拍卖师开始拍品的拍卖
     */
    setGoodStart:function (data) {
        window.location.reload();
        // myConfig.good_info.is_pause=1;
        // //alert('拍卖师开始拍品的拍卖！'+ data.is_pause);
        // // FuZhi.set_apply_show(false);
        // // FuZhi.set_sendprice_show(true);
        // FuZhi.set_sendPricedisable(false,'出价');
        // //状态赋值到页面
        // if(data.tag==0){//第一次点击拍品开始
        //     alert('第一次点击开始'+data.tag);
        //     myConfig.good_status_record=0;//1代表有0代表没有
        // }else if(data.tag==1 || data.tag==2){
        //     myConfig.good_status_record=1;//1代表有0代表没有mm
        //     if(data.tag == 1)
        //         myConfig.record_cuttime.freetime=data.freetime;
        //     else if(data.tag == 2)
        //         myConfig.record_cuttime.cuttime=data.cuttime;
        //     myConfig.good_status_info.cretime=data.cretime;
        //     myConfig.good_status_info.freetime=data.freetime;
        //     myConfig.good_status_info.cuttime=data.cuttime;
        //     myConfig.good_status_info.oktime=data.oktime;
        //     myConfig.good_status_info.tag=data.tag;
        // }else{//data.tag == 3 代表是第二次点击开始但是没有状态记录
        //     myConfig.good_status_record=0;//1代表有0代表没有
        // }
        // myConfig.servertime=data.servertime;
        // alert('重新初始化');
        // startInit.init();
    },
    /**
     * type为end
     * 拍品已经结束
     */
    setGoodEnd:function (data) {
        alert(data.msg);
        window.location.reload();//刷新
    },
    setGoodGiveup:function (data) {
        myConfig.good_info.is_end=1;
        //alert(data.msg);
        startInit.init();
    },


};
//常用工具
var Tools={
    /**
     * 时间戳转换日期
     * @param <int> unixTime  待时间戳(秒)
     * @param <bool> isFull  返回完整时间(Y-m-d 或者 Y-m-d H:i:s)
     */
    UnixToDate: function(unixTime, isFull) {
        var time = new Date(unixTime * 1000);
        var ymdhis = "";
        ymdhis += time.getUTCFullYear() + "-";
        if((time.getUTCMonth()+1)<=9)
            ymdhis += "0"+ (time.getUTCMonth()+1) + "-";
        else
            ymdhis += (time.getUTCMonth()+1) + "-";
        if(time.getUTCDate()<=9)
            ymdhis += "0"+time.getUTCDate();
        else
            ymdhis += time.getUTCDate();
        if (isFull === true)
        {
            if( time.getUTCHours()<=9)
                ymdhis += " " +"0"+ time.getUTCHours() + ":";
            else
                ymdhis += " " + time.getUTCHours() + ":";
            if(time.getUTCMinutes()<=9)
                ymdhis += "0"+time.getUTCMinutes() + ":";
            else
                ymdhis += time.getUTCMinutes() + ":";
            if(time.getUTCSeconds()<=9)
                ymdhis +="0"+time.getUTCSeconds();
            else
                ymdhis += time.getUTCSeconds();
        }
        return ymdhis;
    },
    /**
     * 也是根据时间戳返回日期格式
     * @param intDiff
     * @returns {string}
     */
    getDate : function (intDiff) {//秒数
        var day = 0,
            hour = 0,
            minute = 0,
            second = 0; //时间默认值
        if (intDiff > 0) {
            day = Math.floor(intDiff / (60 * 60 * 24));
            hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
            minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
            second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
        }
        if (hour <= 9) hour = '0' + hour;
        if (minute <= 9) minute = '0' + minute;
        if (second <= 9) second = '0' + second;
        var timestr=day+"天"+hour+"时"+minute+"分"+second+"秒";
        return timestr;
    },
    /**
     * 判断是否定义了该值，针对服务器输出的变量
     * @returns {number}
     */
    isDingyi : function () {
        var a = arguments[0] ? arguments[0] : 0;
        return a;
    },
    /**
     * 清除定时器
     * @param interval
     */
    myClearInterval : function (interval) {//清除定时器
        clearInterval(interval);
    },
    /**
     * 验证手机号 返回布尔值
     * @param phone
     * @returns {boolean}
     */
    isPhoneNo : function (phone) {
        var pattern = /^1[34578]\d{9}$/;
        return pattern.test(phone);
    },
    /**
     * 设置结束
     */
    setEnd:function () {
        alert('该拍品已经结束');
        FuZhi.set_time_str('已结束','00天00时00分00秒');
        myConfig.good_info.is_end=1;//拍品结束标志
        FuZhi.set_applydisable(true,'拍品已结束');//设置出价按钮灰色不可以点击
        FuZhi.set_sendPricedisable(true,'拍品已结束');//设置申请资格按钮灰色不可以点击
    },
    /**
     * 设置放弃
     */
    setGiveup:function () {
        alert('该拍品已经被放弃');
        myConfig.good_info.is_giveup=1;
        FuZhi.set_applydisable(true,'拍品被放弃');//设置出价按钮灰色不可以点击
        FuZhi.set_sendPricedisable(true,'拍品被放弃');//设置申请资格按钮灰色不可以点击
    },
    /**
     * 设置停止
     */
    setStop:function () {
        alert('该拍品已经被暂停，请等待拍卖师操作');
        FuZhi.set_applydisable(true,'请等待');
        FuZhi.set_sendPricedisable(true,'请等待');//设置出价按钮灰色不可以点击
        //FuZhi.set_applydisable();//被停止可以申请拍卖资格
    },
    /**
     * 设置停止
     */
    setWait:function () {
        alert('该拍品未开始，请等待！');
        FuZhi.set_applydisable(true,'请等待拍品开始');
        FuZhi.set_sendPricedisable(true,'请等待拍品开始');//设置出价按钮灰色不可以点击
        //FuZhi.set_applydisable();//被停止可以申请拍卖资格
    },
    /**
     * 恢复按钮
     */
    recoverBtn:function () {
        FuZhi.set_applydisable(false,'申请资格');//设置出价按钮灰色不可以点击
        //alert('恢复');
        FuZhi.set_sendPricedisable(false,'出价');//设置申请资格按钮灰色不可以点击
    },

};
//主要用于判断
var judgeTools={
    //判断拍品是否结束
    judge_end:function () {
        if(myConfig.good_info.is_end ==1 )
            return true;
        else
            return false;
    },
    //判断 是否为自己拍
    judge_current:function () {
        if(myConfig.auct_info.goods_id==myConfig.good_info.goods_id){
            return true;
        }else{
            return false;
        }
    },
    //判断拍卖会是否开始
    judge_auction_start:function () {
        var tag= myConfig.auct_info.starttime - myConfig.servertime;
        if(tag > 0){
            myConfig.record_cuttime.pmhtime=tag;//设置拍卖会倒计时剩余的秒数
            myConfig.current_stage=0;//设置当前阶段为拍卖会倒计时阶段
            return false;
        }
        else{
            return true;
        }
    },
    //判断是否为自由竞价(如果是设置剩余的竞价时间，如果不是还要设置限时竞价的时间)
    judge_free:function () {
        if(myConfig.good_status_record == 1){//有状态记录
            if(myConfig.good_status_info.tag==1){//是自由竞价
                var mytime=parseInt(myConfig.good_status_info.oktime+myConfig.good_status_info.freetime)-myConfig.servertime;
                //是否为自由竞价
                if(mytime>0){
                    myConfig.record_cuttime.freetime=mytime;//设置自由竞价剩余的秒数
                    myConfig.current_stage=1;//设置当前阶段为自由竞价
                    return true;//是自由竞价
                }else{//一定是限时竞价 mytime为负数
                    myConfig.current_stage=2;//限时竞价
                    //计算并设置出限时竞价剩余的秒数here
                    if(myConfig.newest_bidden_info.tag==1){//出价记录自由竞价，说明限时阶段未出价，且未暂停过（因为最近的状态记录为自由竞价）
                        //自由竞价的剩余时间就是上面的cuttime - |mytime|绝对值
                        myConfig.record_cuttime.cuttime=myConfig.good_info.cuttime+mytime;
                    }else if(myConfig.newest_bidden_info.tag==2){
                        //最新的出价为限时竞价 ：(出价时间+限时竞价的时间)-服务器的时间
                        myConfig.record_cuttime.cuttime=(myConfig.newest_bidden_info.cretime+myConfig.good_info.cuttime)-myConfig.servertime;
                    }
                    return false;
                }
            }else {//状态记录为限时竞价
                //计算并设置出限时竞价的秒数
                myConfig.current_stage=2;//限时竞价
                if(myConfig.good_bidden_status==1){//拍品有出价
                    //这里不用考虑出价是否为限时的出价（因为状态记录为限时，如果出价的记录时间比这个早直接用状态记录计算时间，如果比它迟，那么肯定是限时出价）
                    if(myConfig.newest_bidden_info.cretime>myConfig.good_status_info.oktime){
                        //出价在状态之后
                        myConfig.record_cuttime.cuttime=(myConfig.newest_bidden_info.cretime+myConfig.good_info.cuttime)-myConfig.servertime;
                    }else{
                        //出价在状态记录之前
                        myConfig.record_cuttime.cuttime=(myConfig.good_status_info.oktime+myConfig.good_status_info.cuttime)-myConfig.servertime;
                    }
                }else{//拍品无出价
                    myConfig.record_cuttime.cuttime=(myConfig.good_status_info.oktime+myConfig.good_status_info.cuttime)-myConfig.servertime;
                }

                return false;
            }
        }else if(myConfig.good_status_record == 0){//无状态记录

            var time=parseInt(myConfig.good_info.starttime+myConfig.good_info.freetime);
            var tag=time - parseInt(myConfig.servertime);
            if(tag > 0){//还处于自由竞价阶段
                myConfig.record_cuttime.freetime=tag;//设置自由竞价剩余的秒数
                myConfig.current_stage=1;//设置当前阶段为自由竞价

                return true;//是自由竞价
            }else{
                myConfig.current_stage=2;//设置当前阶段为限时竞价
                if(myConfig.good_bidden_status==1){//代表有出价记录
                    if(myConfig.newest_bidden_info.tag==1){//出价记录自由竞价，说明限时阶段未出价，且未暂停过
                        //自由竞价的剩余时间就是上面的cuttime - |mytime|绝对值
                        myConfig.record_cuttime.cuttime=myConfig.good_info.cuttime+tag;
                    }else if(myConfig.newest_bidden_info.tag==2){
                        //最新的出价为限时竞价 ：(出价时间+限时竞价的时间)-服务器的时间
                        myConfig.record_cuttime.cuttime=(myConfig.newest_bidden_info.cretime+myConfig.good_info.cuttime)-myConfig.servertime;
                    }
                }else{//无出价 限时剩余秒数=(拍品的限时的秒数-自由竞价超出的秒数)因为这里超出的秒数为负数，所以直接加
                    myConfig.record_cuttime.cuttime=myConfig.good_info.cuttime+tag;
                }
                return false;//应该是限时竞价
            }
        }
    },
    //判断拍品是否停止
    judge_stop:function () {
        if(myConfig.good_info.is_pause==0){//0表示暂停1为开始
            return true;
        }else{
            return false;
        }
    },
    //判断是否被放弃
    judge_giveup:function () {
        if(myConfig.good_info.is_giveup==1){//1为放弃
            return true;
        }else{
            return false;
        }
    },

};
var startInit={
    //逻辑启动
    init:function () {
        if(judgeTools.judge_end()){//结束
            Tools.setEnd();
            //alert('结束');
        }else{//未结束
            //alert('未结束');
            if(judgeTools.judge_giveup()){//放弃了该拍品
                Tools.setGiveup();
            }else{//未放弃
                //alert('未放弃');
                if(!judgeTools.judge_current()){//是否为自己拍
                    //不是是本拍品拍(显示申请拍卖资格，隐藏出价)
                    //alert('不是自己被拍卖');
                    FuZhi.set_apply_show(true);
                    FuZhi.set_sendprice_show(false);
                    //如果拍卖资格审核已经通过的话
                    if(myConfig.user_info.status==1){
                        FuZhi.set_applydisable(true,'请等待开始！');
                    }
                    if(judgeTools.judge_auction_start()){
                        //alert('拍卖会已经开始');
                    }else{//拍卖会未开始显示拍卖会倒计时
                        //alert('here');
                        myInterval.startInterval();
                    }
                }else{//是本拍品拍
                    //拍卖会是否开始
                    if(judgeTools.judge_auction_start()){//已经开始
                        //alert('拍卖会开始');
                        if(judgeTools.judge_stop()){//是否被暂停
                            //alert('拍卖被停止或未开始');
                            if(myConfig.good_info.starttime==null){
                                alert('拍品未开始，请等待！');
                            }else{
                                if(myConfig.good_status_info.tag==1){
                                    //自由竞价
                                    var str=Tools.getDate(myConfig.good_status_info.freetime);
                                    FuZhi.set_time_str('自由竞价',str);
                                }else if(myConfig.good_status_info.tag==2){
                                    //限时竞价
                                    var str=Tools.getDate(myConfig.good_status_info.cuttime);
                                    FuZhi.set_time_str('限时竞价',str);
                                }
                                Tools.setStop();
                            }
                        }else{//未被暂停
                            //alert('拍品未被停止！');
                            if(judgeTools.judge_free()){//是自由竞价
                                FuZhi.set_apply_show(false);
                                FuZhi.set_sendprice_show(true);
                                Tools.recoverBtn();
                                myInterval.freeInterval();
                            }else{//不是自由竞价,那必定是限时竞价
                                //设置限时竞价剩余的秒数（该步骤在判断是否为自由竞价那里已经设置好）
                                //开始限时竞价倒计时
                                FuZhi.set_apply_show(false);
                                FuZhi.set_sendprice_show(true);
                                myInterval.cutInterval();
                                Tools.recoverBtn();

                            }
                        }
                    }else {//还未开始
                        //设置拍卖会倒计时剩余的时间(该步骤在判断拍卖会是否开始时候已经设置好)
                        myInterval.startInterval();
                    }
                }
            }

            //不管什么情况，只要拍品未结束未放弃就可以申请资格
            if(myConfig.user_info.status===0){//等待审核
                alert('拍卖资格等待审核中！');
                FuZhi.set_sendprice_show(false);
                FuZhi.set_apply_show(true);
                FuZhi.set_applydisable(true,'等待审核');
                myInterval.applyInterval();
            }else if(myConfig.user_info.status==1){//通过
                //FuZhi.set_applydisable(true,'等待拍品开始');
                //FuZhi.set_sendprice_show(true);
                //uZhi.set_apply_show(false);
            }else if(myConfig.user_info.status==2){//被驳回
                alert('申请被驳回，请重新申请！');
                Tools.recoverBtn();
                FuZhi.set_sendprice_show(false);
                FuZhi.set_apply_show(true);
            }else if(myConfig.user_info.status==null){//还未申请
                // alert('未申请拍卖资格');
                FuZhi.set_sendprice_show(false);
                FuZhi.set_apply_show(true);
                Tools.recoverBtn();
            }
        }
    },
    //赋值
    initInfo:function (good_info,auct_info,user_info,good_status_info,newest_bidden_info,other_info) {
        startInit.initGoodInfo(good_info);
        startInit.initAuctInfo(auct_info);
        startInit.initUserInfo(user_info);
        if(good_status_info){//不为空的话
            startInit.initGoodStatusInfo(good_status_info);
            myConfig.good_status_record=1;//有状态记录
        }
        if(newest_bidden_info){//有出价信息
            startInit.initNewestBiddenInfo(newest_bidden_info);
            myConfig.good_bidden_status=1;
        }
        myConfig.servertime=other_info.servertime;
    },
    //拍品基本信息赋值 data为json字符串
    initGoodInfo:function (data) {
        myConfig.good_info.goods_id= data.id;
        myConfig.good_info.max_price=data.max_price;
        myConfig.good_info.start_price=data.start_price;
        myConfig.good_info.marup_price=data.marup_price;
        myConfig.good_info.end_price=data.end_price;
        myConfig.good_info.cuttime=data.cuttime;
        myConfig.good_info.freetime=data.freetime;
        myConfig.good_info.is_end=data.is_end;
        myConfig.good_info.auct_id=data.auct_id;
        myConfig.good_info.is_giveup=data.is_giveup;
        myConfig.good_info.is_pause=data.is_pause;
        myConfig.good_info.is_sure=data.is_sure;
        myConfig.good_info.sort=data.sort;
        myConfig.good_info.starttime=data.starttime;
    },
    //拍卖会信息赋值
    initAuctInfo:function (data) {
        //alert(data.goods_id);
        myConfig.auct_info.goods_id=data.goods_id;//拍卖会当前的拍品id
        myConfig.auct_info.starttime=data.starttime;
    },
    //用户信息赋值
    initUserInfo:function (data) {
        myConfig.user_info.user_id=data.user_id;
        myConfig.user_info.usernumber=data.usernumber;
        myConfig.user_info.phone=data.phone;
        myConfig.user_info.status=data.status;
        //alert(data.usernumber);
    },
    //拍品状态赋值
    initGoodStatusInfo:function (data) {
        myConfig.good_status_info.cretime=data.cretime;
        myConfig.good_status_info.oktime=data.oktime;
        myConfig.good_status_info.tag = data.tag;
        myConfig.good_status_info.cuttime=data.cuttime;
        myConfig.good_status_info.freetime=data.freetime;
    },
    //最新竞价记录赋值
    initNewestBiddenInfo:function (data) {
        myConfig.newest_bidden_info.tag=data.tag;
        myConfig.newest_bidden_info.cretime=data.cretime;
        myConfig.newest_bidden_info.price=data.price;
        myConfig.newest_bidden_info.user_id=data.user_id;
        myConfig.newest_bidden_info.usernumber=data.usernumber;
    }

};
$(function () {
    myTab.init();//初始化标签页
    //alert((7-14)+5);
    WsTools.init();
});
