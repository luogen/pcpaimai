<?php
function httpsGet($url){
    $curl = curl_init();  
    curl_setopt($curl,CURLOPT_URL,$url);
    curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.96 Safari/537.36');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);  
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);// https请求不验证证书和hosts  
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    
    $res=json_decode(curl_exec($curl),true);  
    curl_close($curl);  
    return $res;  
}  
 function getRandCode($num=20){
        $array=array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            '0','1','2','3','4','5','6','7','8','9'
        );
        $tmpstr='';
        for ($i=1;$i<=$num;$i++){
            $key=rand(0,count($array));
            $tmpstr.=$array[$key];
        }
        return $tmpstr;
  }
ini_set("error_reporting","E_ALL & ~E_NOTICE");
$url="https://www.cp91.com/data/jspk10/lotteryList/".date('Y-m-d',time()).'.json?'.getRandCode();
$res=httpsGet($url);
$res[0]['preIssue']= $res[0]['issue'];
//$res[0]['currentOpenDateTime']=(strtotime($res[0]['openDateTime'])*1000);
$res[0]['currentOpenDateTime']=(strtotime($res[0]['openDateTime']));
$res[0]['mytime']=date('Y-m-d h:i:s',strtotime($res[0]['openDateTime']));
 $res=json_encode($res[0]);
 echo $res;