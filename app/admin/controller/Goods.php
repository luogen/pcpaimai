<?php
namespace app\admin\controller;
use think\Db;
use clt\Leftnav;
class Goods extends Common{
    public function test(){
        $info= Db::table('clt_users')->
        field('openid,nickname')
            ->where('id',2421)
            ->find();
        var_dump($info);
    }
    public function getMaxPrice($list){
        foreach ($list['data'] as $k=>$v){
            $info= Db::table('clt_bidden')->
                field('price,usernumber')
                ->where('goodsid',$v['id'])
                ->order('price desc')
                ->select();
            $info=$info[0];
            if($info){
                $list['data'][$k]['maxprice']=$info['price'];
                $list['data'][$k]['usernumber']=$info['usernumber'];
                $info= Db::table('clt_qualify')->
                field('userid,username,phone')
                    ->where('usernumber',$info['usernumber'])
                    ->find();
                if($info){
                    $list['data'][$k]['userid']=$info['userid'];
                    $list['data'][$k]['username']=$info['username'];
                    $list['data'][$k]['phone']=$info['phone'];
                    $info= Db::table('clt_users')->
                    field('openid,nickname')
                        ->where('user_id',$info['userid'])
                        ->find();

                    if($info){
                        $list['data'][$k]['nickname']=utf8_decode($info['nickname']);
                        $list['data'][$k]['openid']=$info['openid'];
                    }
                }
            }else{
                $list['data'][$k]['maxprice']= null;
            }
        }
        return ($list);

    }
    //进行中的商品
	public function current(){
        if(request()->isPost()) {
            $page =input('pageIndex');
            $pageSize =input('pageSize');
            $list = Db::table('clt_salegoods')->alias('k')
                ->field('k.id,k.title,k.thumb,k.endtime,k.starttime,k.startprice,k.marupprice,k.endprice,k.status')
                ->where('isend',0)
                ->where('status',1)
                ->order('k.id asc')
                ->cache(false)
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            $list=$this->getMaxPrice($list);
//            // 模板变量赋值
            $rsult['list'] = $list['data'];
           $rsult['count'] = $list['total'];
           $rsult['rel'] = 1;
            echo json_encode($rsult);
            exit;
        }
        return $this->fetch();
    }
    //商品的发布于撤回
    public function goodsState(){
        $id=input('post.id');
        $status=db('salegoods')->where(array('id'=>$id))->value('status');//判断当前状态情况
        if($status==1){
            $data['status'] = 0;
            db('salegoods')->where(array('id'=>$id))->setField($data);
            $result['info'] = '已撤回';
            $result['status'] = 1;
        }else{
            $data['status'] = 1;
            db('salegoods')->where(array('id'=>$id))->setField($data);
            $result['info'] = '已发布';
            $result['status'] = 1;
        }
        return $result;
    }

    //商品的流拍与成交
    public function goodsLiupai(){
        $id=input('post.id');
        $status=db('salegoods')->where(array('id'=>$id))->value('issure');//判断当前状态情况
        if($status==1){
            $data['issure'] = 0;
            db('salegoods')->where(array('id'=>$id))->setField($data);
            $result['info'] = '已流拍';
            $result['status'] = 1;
        }else{
            $data['issure'] = 1;
            db('salegoods')->where(array('id'=>$id))->setField($data);
            $result['info'] = '已成交';
            //这里给用户发送确认消息
	     // $wechat=controller('Wechat');
           // $wechat->sendUser($userid,$id);
            $result['status'] = 1;
        }
        return $result;
    }

    //打印
    public function printInfo(){
        $goodsid=input('get.id');
        $userid=input('get.userid');
        $usernumber=input('get.usernumber');
        $goods_info=Db::table('clt_salegoods')
            ->field('id,title,startprice,endprice,thumb,starttime,auct_company,phone')
            ->where('id',$goodsid)
            ->find();
//        $biddenlist=Db::table('clt_bidden')
//            ->field('id,usernumber,price,cretime')
//            ->where('goodsid',$goodsid)
//            ->order('price desc')
//            ->select();
        $biddenlist=Db::table('clt_qualify')->alias('k')
            ->join('clt_bidden w','k.usernumber=w.usernumber')
            ->field('w.id,w.usernumber,w.price,w.cretime,k.username,k.phone')
            ->where('k.goodsid',$goodsid)
            ->where('w.goodsid',$goodsid)
            ->order('price asc')
            ->select();
         $count=count($biddenlist) -1;
        $goods_info['maxprice']=$biddenlist[$count]['price'];
             $qualifyinfo=Db::table('clt_qualify')
            ->field('id,username,phone,userid')
            ->where('usernumber',$usernumber)
            ->find();
       // var_dump($biddenlist);
        $this->assign('goods_info',$goods_info);
        $this->assign('biddenlist',$biddenlist);
        $this->assign('qualifyinfo',$qualifyinfo);
        return $this->fetch();
    }
	public function sendMsgToUser()	{
	   $goodsid=input('get.goodsid');
           $userid=input('get.userid');
	     $wechat=controller('admin/Wechat');
        $wechat->sendUser($userid,$goodsid);

      }
    //删除商品，以及对应的用户资格，出价记录
    public function delGoods(){
        $id=input('get.id');
        Db::table(config('database.prefix').'salegoods')
            ->where('id',$id)
            ->delete();
        Db::table(config('database.prefix').'bidden')
            ->where('goodsid',$id)
            ->delete();
        Db::table(config('database.prefix').'qualify')
            ->where('goodsid',$id)
            ->delete();
        $this->success('删除成功');
    }
    //已经结束的商品
    public function haveend(){
        if(request()->isPost()) {
            $page =input('pageIndex');
            $pageSize =input('pageSize');
            $list = Db::table('clt_salegoods')->alias('k')
                ->field('k.id,k.title,k.thumb,k.endtime,k.starttime,k.startprice,k.marupprice,k.endprice,k.issure')
                ->where('isend',1)
                ->order('k.endtime desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            $list=$this->getMaxPrice($list);
//            // 模板变量赋值
            $rsult['list'] = $list['data'];
            $rsult['count'] = $list['total'];
            $rsult['rel'] = 1;
            echo json_encode($rsult);
            exit;
        }
        return $this->fetch();
    }

    //未发布的商品（）
    public function unpublish(){
        if(request()->isPost()) {
            $page =input('pageIndex');
            $pageSize =input('pageSize');
            $list = Db::table('clt_salegoods')->alias('k')
                ->field('k.id,k.title,k.thumb,k.endtime,k.starttime,k.startprice,k.marupprice,k.endprice,k.status')
                ->where('isend',0)
                ->where('status',0)
                ->order('k.id asc')
                ->cache(false)
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            $list=$this->getMaxPrice($list);
//            // 模板变量赋值
            $rsult['list'] = $list['data'];
            $rsult['count'] = $list['total'];
            $rsult['rel'] = 1;
            echo json_encode($rsult);
            exit;
        }
        return $this->fetch();
    }

}
