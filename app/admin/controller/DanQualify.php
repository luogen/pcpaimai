<?php
namespace app\admin\controller;
use think\Db;
use clt\Leftnav;
class DanQualify extends Common{
    //
    public function index(){
        if(request()->isPost()) {
            $page =input('pageIndex');
            $pageSize =200;//这个按要求打印所有审核信息，那就一页显示所有
            $goodsid=session('goodsid');
            $status = isset($_POST['status'])?$_POST['status']:-1;
            $where['goods_id'] = $goodsid;
            if($status == -1){
                $list = Db::table('clt_dan_qualify')
                    ->where($where)
                    ->where('status','NEQ',2)//被驳回的不列出来
                    ->order('cretime desc')
                    ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                    ->toArray();
            }else{
                $list = Db::table('clt_dan_qualify')
                    ->where($where)
                    ->where('status','EQ',1)
                    ->where('status','NEQ',2)//被驳回的不列出来
                    ->order('cretime desc')
                    ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                    ->toArray();
            }
//            // 模板变量赋值
            $rsult['list'] = $list['data'];
            $rsult['status'] = $status;
            $rsult['count'] = $list['total'];
            $rsult['rel'] = 1;
            echo json_encode($rsult);
            exit;
        }else{
            $goodsid=input('get.goodsid');
            $this->assign('goodsid',$goodsid);
            session('goodsid',$goodsid);
        }

        return $this->fetch();
    }
    //拍卖资格的审核
    public function qualifyState(){
        $id=input('post.id');
        $status=db('dan_qualify')->where(array('id'=>$id))->value('status');//判断当前状态情况
        if($status==1){
            $data['status'] = 0;
            db('dan_qualify')->where(array('id'=>$id))->setField($data);
            $result['info'] = '未审核';
            $result['status'] = 1;
        }else{
            $data['status'] = 1;
            db('dan_qualify')->where(array('id'=>$id))->setField($data);
            $result['info'] = '已审核';
            $result['status'] = 1;
        }
        return $result;
    }

//驳回资格
    public function usersBohui($qualifyid=''){
        if($qualifyid){
            $data['status']=2;
            $tag=Db::table('clt_dan_qualify')
                ->where('id',$qualifyid)
                ->update($data);
            if($tag){
                $this->success('驳回成功');
            }else{
                $this->error('出错了，请重试');
            }
        }else{
            $this->error('错误操作');
        }
    }

}
