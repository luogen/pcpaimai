<?php
namespace app\admin\controller;
use think\Db;
use clt\Leftnav;
class Qualify extends Common{
    //拍品列表
    public function index(){
        if(request()->isPost()){
            $key=input('post.key');
            $page =input('pageIndex');
            $pageSize =input('pageSize');
            $list=db('salegoods')->alias('k')
                ->field('k.id,k.isend,k.title,k.thumb,k.endtime,k.starttime,k.startprice,k.marupprice,k.endprice,k.status')
                ->where('k.title|k.id','like',"%".$key."%")
                ->order('k.starttime desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            return $result = ['code'=>0,'msg'=>'获取成功!','list'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }
    public function getGoodsInfo($list){
        foreach ($list['data'] as $k=>$v){
            $info= Db::table('clt_salegoods')->
            field('title,thumb')
                ->where('id',$v['goodsid'])
                ->find();
            if($info){
                $list['data'][$k]['thumb']=$info['thumb'];
                $list['data'][$k]['title']=$info['title'];
            }
        }
        return ($list);
    }
    //
	public function qualify(){
        if(request()->isPost()) {
            $page =input('pageIndex');
            $pageSize =200;//这个按要求打印所有审核信息，那就一页显示所有
            $goodsid=session('goodsid');
			$status = isset($_POST['status'])?$_POST['status']:-1;
			if($status != -1){
				$where['status'] = $status;
			}
			$where['goodsid'] = $goodsid;
            $list = Db::table('clt_qualify')
                ->where($where)
                ->order('status asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            // 模板变量赋值
			
            $list= $this->getGoodsInfo($list);
			$rsult['status'] = $status;
            $rsult['list'] = $list['data'];
           $rsult['count'] = $list['total'];
           $rsult['rel'] = 1;
            echo json_encode($rsult);
            exit;
        }else{
            $goodsid=input('get.goodsid');
            session('goodsid',$goodsid);
        }
		
        return $this->fetch();
    }
    //拍卖资格的审核
    public function qualifyState(){
        $id=input('post.id');
        $status=db('qualify')->where(array('id'=>$id))->value('status');//判断当前状态情况
        if($status==1){
            $data['status'] = 0;
            db('qualify')->where(array('id'=>$id))->setField($data);
            $result['info'] = '未审核';
            $result['status'] = 1;
        }else{
            $data['status'] = 1;
            db('qualify')->where(array('id'=>$id))->setField($data);
            $result['info'] = '已审核';
            $result['status'] = 1;
        }
        return $result;
    }
//驳回资格
    public function usersBohui($usernumber=''){
        if($usernumber){
            $tag=Db::table('clt_qualify')
                ->where('usernumber',$usernumber)
                ->delete();
            if($tag){
                $this->success('驳回成功');
            }else{
                $this->error('出错了，请重试');
            }
        }else{
            $this->error('错误操作');
        }
    }

}
