<?php
namespace app\admin\controller;
use think\Db;
use clt\Leftnav;
use think\Request;

class WebAuction extends Common
{
    /**
     * 拍卖会首页（按创建时间倒序展示所有的拍卖会）
     * @access public
     * @return mixed
     */
    public function index()
    {
        if (request()->isPost()) {
            $page = input('pageIndex');
            $pageSize = input('pageSize');
            $admin_id=session('aid');
            $list=array();
            if($admin_id == 1){
                //超级管理员
                $list = Db::table(config('database.prefix') . 'web_auction')
                    ->order('cretime DESC')
                    ->paginate(array('list_rows' => $pageSize, 'page' => $page))
                    ->toArray();
            }else{
                //拍卖师
                $list = Db::table(config('database.prefix') . 'web_auction')
                    ->where('auctor_id',$admin_id)//只能看到属于自己的拍卖会
                    ->order('cretime DESC')
                    ->paginate(array('list_rows' => $pageSize, 'page' => $page))
                    ->toArray();
            }
            // 模板变量赋值
            $rsult['list'] = $list['data'];
            $rsult['count'] = $list['total'];
            $rsult['rel'] = 1;
            echo json_encode($rsult);
            exit;
        }
        return $this->fetch();
    }

    public function test(){
        var_dump(session('acution_info'));
    }
    /**
     * 添加拍卖会
     * @access public
     * @return mixed
     */
    public function addAuct()
    {
        if(request()->isPost()) {
            $data =  input('post.');
            //添加模式
            $data['cretime'] = time();
            $pid = explode(':',$data['auctor_id']);//取出拍卖师的id
            $data['auctor_id'] =$pid[1]?$pid[1]:0;
            $data['starttime']=strtotime($data['starttime']);
            db('web_auction')->insert($data);
            $result['code'] = 1;
            $result['msg'] = '添加成功!';
            $result['url'] = url('index');
            return $result;
        }else{//group_id拍卖师角色的id
            $auctorList=db('admin')->where('group_id',3)->order('admin_id desc')->select();//查询拍卖师
            $this->assign('auctorList',json_encode($auctorList,true));
            $this->assign('title', lang('add') . '拍卖会');
            $this->assign('info', 'null');
            return $this->fetch('auctForm');
        }
    }

    /**
     * 修改拍卖会内容
     * @access public
     * @param integer $id
     * @return mixed
     */
    public function editAuct($id)
    {
        if(request()->isPost()) {
            $map['id'] = input('post.id');
            $data =  input('post.');
            //添加模式
            $pid = explode(':',$data['auctor_id']);//取出拍卖师的id
            $data['auctor_id'] =$pid[1]?$pid[1]:0;
            $data['starttime']=strtotime($data['starttime']);
            $data['pid'] = db('web_auction')->where($map)->update($data);
            $result['code'] = 1;
            $result['msg'] = '修改成功!';
            $result['url'] = url('index');
            return $result;
        }else{
            $auctorList=db('admin')->where('group_id',3)->order('admin_id desc')->select();//查询拍卖师
            $this->assign('auctorList',json_encode($auctorList,true));
            $info = db('web_auction')->where('id',input('id'))->find();
            $this->assign('title', lang('edit') . '拍卖会');
            $this->assign('info',json_encode($info,true));
            return $this->fetch('auctForm');
        }
    }

    /**
     * 根据拍卖会的id删除拍卖会，以及拍卖会下所有的拍品以及拍品下边的竞价记录和编号
     * @param $id 要删除的拍卖会的第id
     */
    public function delAuct()
    {
        $id = input('id');//拍卖会id
        $good=db('web_goods');
        $bidden=db('web_bidden');
        $qualify=db('web_qualify');
        $words=db('web_words');
        $ids=$good->where('auct_id',$id)->field('id')->select();
        foreach ($ids as $k => $v){
            $bidden->where(array('goods_id'=>$v['id']))->delete();
            $qualify->where(array('goods_id'=>$v['id']))->delete();
            $words->where(array('goods_id'=>$v['id']))->delete();
        }
        $good->where('auct_id',$id)->delete();//删除拍品
        db('web_auction')->where('id',$id)->delete();
        $this->redirect('index');
    }



}
