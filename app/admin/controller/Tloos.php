<?php
namespace app\admin\controller;
//use think\{Controller,Db,Input};//
use think\Controller;
use think\Db;
use Aliyun\DySDKLite\Sms\SmsApi;

class Tloos extends Common{
    protected $sms;
    public function _initialize(){
        parent::_initialize();
        $this->sms=new SmsApi("your access key", "your access key secret");
    }

    public function sendSms(array $msg)
    {

        $response = $this->sms->sendSms(
            "短信签名", // 短信签名
            "SMS_0000001", // 短信模板编号
            "12345678901", // 短信接收者
            Array (  // 短信模板中字段的值
                "code"=>"12345",
                "product"=>"dsd"
            ),
            "123"   // 流水号,选填
        );
        return $response;
    }

    /**
     * 产生验证码并session
     * @return int
     */
    public function getCode(){
        $num = rand(100000,999999);
        session('code',$num);
        return $num;
    }


}