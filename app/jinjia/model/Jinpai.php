<?php
/**
 * Created by PhpStorm.
 * User: luobiao
 * Date: 2017/11/1
 * Time: 15:59
 */
namespace app\jinjia\model;
use think\Model;

class Jinpai extends Model
{
    /**
     * 根据拍品id获取按顺序（sort）同场拍卖会下所有拍品,以及当前拍品
     * @param $id 拍品id
     * @return mixed
     */
    public function getGoods($id)
    {
        $goods=db('web_goods');
        $current_good=$goods->find($id);//当前的拍品信息
        //同场拍卖会上的拍品
        $goodslist=$goods->where('auct_id',$current_good['auct_id'])->order('sort asc')->select();
        //拍卖会的信息
        $auct_info=db('web_auction')->find($current_good['auct_id']);
        $mygoods['current']=$current_good;
        $mygoods['goodslist']=$goodslist;
        $mygoods['auct_info']=$auct_info;
        return $mygoods;
    }

    public function dangetGoods($id)
    {
        $goods=db('dan_goods');
        $current_good=$goods->find($id);//当前的拍品信息
        $mygoods['current']=$current_good;
        return $mygoods;
    }

    /**
     * 根据拍品id获取该拍品下的拍卖师发言
     * @param $id
     * @return array
     */
    public function getWords($id)
    {
        $wordslist=db('web_words')->where('goods_id',$id)->order('cretime desc')->select();
        return $wordslist;
    }
    public function dangetWords($id)
    {
        $wordslist=db('dan_words')->where('goods_id',$id)->order('cretime desc')->select();
        return $wordslist;
    }
    /**
     * 根据拍品id获取该拍品下的所有出价记录
     * @param $id
     * @return array
     */
    public function getBidden($id)
    {
        $biddenlist=db('web_bidden')->where('goods_id',$id)->order('cretime desc')->select();
        return $biddenlist;
    }
    public function dangetBidden($id)
    {
        $biddenlist=db('dan_bidden')->where('goods_id',$id)->order('cretime desc')->select();
        return $biddenlist;
    }

    /**
     * 获取用户最新的一次资格申请
     * @param $id 拍品id
     * @param $uid 用户pc登录后的session用户id
     * @return array
     */
    public function getUsernumber($id,$uid)
    {
        //可能申请过多次（之前可能被驳回），选取最新的一次
        $qualify_info=db('web_qualify')->where('goods_id',$id)->where('user_id',$uid)->order('cretime desc')->find();
        return $qualify_info;
    }

    public function dangetUsernumber($id,$uid)
    {
        //可能申请过多次（之前可能被驳回），选取最新的一次
        $qualify_info=db('dan_qualify')->where('goods_id',$id)->where('user_id',$uid)->order('cretime desc')->find();
        return $qualify_info;
    }

    /**
     * 申请拍卖资格
     * @param $data 数组包含差入信息
     * @return mixed
     */
    public function apply($data)
    {
        $data['cretime']=time();
        $data['usernumber']=$this->shuzi($data['goods_id']);
        $id=db('web_qualify')->insertGetId($data);
        if($id){//申请成功
            $info['status']='ok';
        }else{//申请失败
            $info['status']='error';
        }
        return $info;

    }
    public function danapply($data)
    {
        $data['cretime']=time();
        $data['usernumber']=$this->shuzi($data['goods_id']);
        $id=db('dan_qualify')->insertGetId($data);
        if($id){//申请成功
            $info['status']='ok';
        }else{//申请失败
            $info['status']='error';
        }
        return $info;

    }
    /**
     * @param $map(用户id和拍品id)
     * @return mixed
     */
    public function applyStatus($map)
    {
        $tag = db('web_qualify')
            ->where($map)
            ->order('cretime desc')
            ->find();
        if ($tag) {
            if($tag['status']==1)//审核通过
                $info['status'] = 'ok';
            else if($tag['status']==2)
                $info['status'] = 'bohui';
            else
                $info['status'] = 'wait';
        }
        return $info;
    }

    public function danapplyStatus($map)
    {
        $tag = db('dan_qualify')
            ->where($map)
            ->order('cretime desc')
            ->find();
        if ($tag) {
            if($tag['status']==1)//审核通过
                $info['status'] = 'ok';
            else if($tag['status']==2)
                $info['status'] = 'bohui';
            else
                $info['status'] = 'wait';
        }
        return $info;
    }
    /**
     * 根据拍品id获取拍卖会信息
     * @param $id
     * @return array
     */
    public function getAuctInfo($id){
        $good_info=db('web_goods')->find($id);
        $auct_info=db('web_auction')->where('id',$good_info['auct_id'])->find();
        return $auct_info;
    }

    /**
     * 根据拍品id获取拍品最新的状态信息
     * @param $id
     * @return array
     */
    public function getGoodStatus($id)
    {
        $status_info=db('web_goodstatus')->where('goods_id',$id)->order('cretime desc')->find();
        return $status_info;
    }

    /**
     * 储存前台提交的拍品状态（对拍品状态每一次只存一次）
     * @param $data
     * @return array $info['status']='ok'|'error'
     */
    public function insertGoodStatus($data)
    {
        $goodStatus=db('web_goodstatus');
        $map['goods_id']=$data['goods_id'];
        $map['unique_str']=$data['unique_str'];
        $status_info=$goodStatus->where($map)->select();
        if($status_info){
            $info['status']='ok';
            return $info;
        }else{
            //根据tag判断是哪一个阶段的剩余时间
            if($data['tag'] == 1)
                $data['freetime']=$data['time'];
            else if($data['tag'] == 2)
                $data['cuttime']=$data['time'];
            $data['cretime']=time();
            $id= $goodStatus->insertGetId($data);
            if($id)
                $info['status']='ok';
            else
                $info['status']='error';
            return $info;
        }
    }

    /**
     * 拍品结束
     * @param $data
     * @return $info['status','msg']
     */
    public function goodFinish($data)
    {//goods_id,max_price,user_id(出价最高的用户的id)
        $good=db('web_goods');
        $good_info=$good->find($data['goods_id']);
        if($good_info['is_end']==1){
            $info['status']='haved';
            return $info;
        }else{
            if($data['max_price'] >= $good_info['end_price']){//成交
                $good_data['get_price']=$data['max_price'];
                $good_data['get_userid']=$data['user_id'];
                $info['status']='success';//编号xxx 竞买者成功竞得
                $user_info=$this->getUsernumber($data['goods_id'],$data['user_id']);
                $info['msg']='拍品已成交，由编号（'.$user_info['usernumber'].'）竞买者成功竞得';
            }else{//流拍
                $good_data['is_sure']=1;//流拍
                $info['status']='failed';
                $info['msg']='拍品已流拍！';
            }
            $good_data['id']=$data['goods_id'];
            $good_data['is_end']=1;
            $good_data['endtime']=time();
            $tag=$good->update($good_data);
            return $info;
        }
    }
    public function dangoodFinish($data)
    {//goods_id,max_price,user_id(出价最高的用户的id)
        $good=db('dan_goods');
        $good_info=$good->find($data['goods_id']);
        if($good_info['is_end']==1){
            $info['status']='haved';
            return $info;
        }else{
            if($data['max_price'] >= $good_info['end_price']){//成交
                $good_data['get_price']=$data['max_price'];
                $good_data['get_userid']=$data['user_id'];
                $info['status']='success';
                $user_info=$this->dangetUsernumber($data['goods_id'],$data['user_id']);
                $info['msg']='拍品已成交，由编号（'.$user_info['usernumber'].'）竞买者成功竞得';
            }else{//流拍
                $good_data['is_sure']=1;//流拍
                $info['status']='failed';
                $info['msg']='拍品已流拍！';
            }
            $good_data['id']=$data['goods_id'];
            $good_data['is_end']=1;
            $good_data['endtime']=time();
            $tag=$good->update($good_data);
            return $info;
        }
    }

    public function goodBidden($data)
    {//user_id,goods_id,price,usernumber,tag
        $good_bidden=db('web_bidden');
        $map['goods_id']=$data['goods_id'];
        $map['price']=$data['price'];
        $bidden_info=$good_bidden->where($map)->find();
        if( $bidden_info){//出价无效
            $info['status']='failed';
        }else{
            $data['cretime']=time();
            $tag=$good_bidden->insertGetId($data);
            if($tag){
                $info['status']='ok';
            }else{
                $info['status']='error';
            }
        }
        return $info;
    }

    public function dangoodBidden($data)
    {//user_id,goods_id,price,usernumber,tag
        $good_bidden=db('dan_bidden');
        $map['goods_id']=$data['goods_id'];
        $map['price']=$data['price'];
        $bidden_info=$good_bidden->where($map)->find();
        if( $bidden_info){//出价无效
            $info['status']='failed';
        }else{
            $data['cretime']=time();
            $tag=$good_bidden->insertGetId($data);
            if($tag){
                $info['status']='ok';
            }else{
                $info['status']='error';
            }
        }
        return $info;
    }
    /**
     * 产生一个唯一的且数据库中不存在的拍品申请资格编号
     * @param $id 拍品id
     * @return int
     */
    public function shuzi($id){//递归产生一个编号
        $num1=rand(1000,2000);
        $num2=rand(2000,3000);
        $num=($num1+$num2)-rand(1,100)+rand(100,200);
        $tag=db('web_qualify')->where('usernumber',$num)->where('goods_id',$id)->select();
        if($tag){//已经存在
            $this->shuzi($id);
        }else{
            return $num;
        }
    }


}