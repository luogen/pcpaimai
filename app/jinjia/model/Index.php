<?php
namespace app\jinjia\model;
use think\Model;
class Index extends Model
{
    /**
     * 正在拍卖的拍品
     * @param  $data
     * @return array()
     */
	public function get_now(){
	    //拍卖会的拍品
		$ids=db('web_auction')->where('starttime','<',time())->field('id,starttime')->select();
		$good=db('web_goods');
		$bidden=db('web_bidden');
        $dan_bidden=db('dan_bidden');
		$now_goods_list=array();
		foreach ($ids as $v){
            $list=$good->where('is_end',0)->where('auct_id',$v['id'])->select();
            foreach ($list as $k => $v1){//给每一个拍品添加上拍卖开始时间，把首页的距离结束改为拍卖开始时间
                $list[$k]['pmhtime']=$v['starttime'];
                $list[$k]['is_auct']=1;
                $list[$k]['maxprice']=$this->getMaxPrice($v1['id'],$bidden);
                $list[$k]['user_count']=$this->getUserCount($v1['id'],$bidden);
            }

            $now_goods_list=array_merge($now_goods_list,$list);
        }
        //单品的拍品
        $map['is_end']=0;//未结束的
        $map['status']=1;//已经发布的
        $dan_list=db('dan_goods')->where($map)->where('starttime','<',time())->field('id,title,thumb1,marup_price,starttime as pmhtime')->select();
        foreach ($dan_list as $k => $v1){
            $dan_list[$k]['maxprice']=$this->getMaxPrice($v1['id'],$dan_bidden);
            $dan_list[$k]['user_count']=$this->getUserCount($v1['id'],$dan_bidden);
            $dan_list[$k]['is_auct']=0;
        }
        $now_goods_list=array_merge($now_goods_list,$dan_list);
		return $now_goods_list;
	}

	public function get_soon()
    {
        $ids=db('web_auction')->where('starttime','>',time())->field('id,starttime')->select();
        $good=db('web_goods');
        $soon_goods_list=array();
        foreach ($ids as $v){
            $list=$good->where('is_end',0)->where('auct_id',$v['id'])->select();
            foreach ($list as $k => $v1){//给每一个拍品添加上拍卖开始时间，把首页的距离结束改为拍卖开始时间
                $list[$k]['pmhtime']=$v['starttime']-time();//距离拍卖会开始的时间
                $list[$k]['is_auct']=1;
            }
            $soon_goods_list=array_merge($soon_goods_list,$list);
        }
        //单品的拍品
        $map['is_end']=0;//未结束的
        $map['status']=1;//已经发布的
        $dan_list=db('dan_goods')->where($map)->where('starttime','>',time())->field('id,title,thumb1,marup_price,starttime as pmhtime')->select();
        foreach ($dan_list as $k => $v1){
            $dan_list[$k]['pmhtime']=$v1['pmhtime']-time();//距离拍卖会开始还有多少秒
            $dan_list[$k]['is_auct']=0;
        }
        $soon_goods_list=array_merge($soon_goods_list,$dan_list);

        return $soon_goods_list;
    }

    /**
     * 根据拍品id获取拍品最高价格
     * @param $id 拍品id
     * @param $model 拍品模型
     * @return int
     */
    public function getMaxPrice($id,$model)
    {
        $list=$model->where('goods_id',$id)->order('cretime desc')->field('price')->limit(1)->select();
        if($list)
            return $list['0']['price'];
        else return 0.00;
    }

    /**
     * 根据拍品id获取拍品加价人数
     * @param $id 拍品id
     * @param $model 拍品模型
     * @return int
     */
    public function getUserCount($id,$model)
    {
        $list=$model->where('goods_id',$id)->distinct(true)->field('user_id')->select();
        return count($list);
    }
}
