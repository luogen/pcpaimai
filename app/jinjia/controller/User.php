<?php
namespace app\jinjia\controller;
use think\Controller;
use think\Model;

class User extends Controller
{
    /**
     * 用户登录
     * @return 提示信息跳转到下一个页面
     */
    public function login(){
        if(request()->isPost()) {
            //$user = new \app\jinjia\model\User();
            $user = model('User');
            $data = input('post.');
            $num = $user->login($data);
            if($num == 1){
               $this->success('登录成功','Index/index');
            }else{
                $this->error('用户名或密码错误');
            }
        }
        return $this->fetch('login');
    }

    /**
     * 用户注册
     * @return 提示信息跳转到下一个页面
     */
    public function regist(){
        if(request()->isPost()) {
            $user = model('User');
            $data = input('post.');
            $tag=$user->regist($data);
            if($tag){
                $this->success('注册成功','login');
            }else{
                $this->error('注册失败，请重试！');
            }
        }
        return $this->fetch('regist');
    }

    /**
     * 注销登录
     */
    public function logout(){
        session(null);
        $this->redirect('user/login');
    }
}