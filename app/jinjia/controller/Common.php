<?php
namespace app\jinjia\controller;
use think\Request;
use think\Db;
use think\Controller;
class Common extends Controller
{
    public function _initialize()
    {
        //判断用户是否登录
        if (!session('pcuser')) {
            $this->redirect('user/login');
        }else{
            $user['nickname']=session('pcuser')['nickname'];
            $user['user_id']=session('pcuser')['user_id'];
            $this->assign('myuser',$user);
            //var_dump($user);
        }
    }
    //空操作
    public function _empty(){
        return $this->error('错误的空操作，返回上次访问页面中...');
    }

}
