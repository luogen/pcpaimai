<?php
namespace app\mobile\model;
use think\Model;
class User extends Model
{
	public function login($data){
		$user=db('users')->where('nickname',$data['nickname'])->field('user_id,nickname,head_pic,mobile,password')->find();
		if($user){
			if($user['password'] == md5($data['password'])){
				session('pcuser',$user);
				return 1; //信息正确
			}else{
				return -1; //密码错误
			}
		}else{
			return -1; //用户不存在
		}
	}
	
	public function regist($data)
    {//暂时抛开了短信验证码
        $userpwd_data['pwd']= $data['password'];//显示一个明文密码，客户要求要看
        $data['password']=md5($data['password']);
        //表明用户的来源为PC注册
        $data['level']=7;
        $data['reg_time']=time();
        $userpwd_data['user_id']=db('users')->insertGetId($data);
        $tag=db('userpwd')->insert($userpwd_data);
        return $tag;
    }

}
