<?php
namespace app\disport\controller;
use think\Input;
use think\Db;
use clt\Leftnav;
use think\Request;
use think\Controller;
class Common extends Controller{
    protected $pagesize;
    public function _initialize(){
        $sys = F('System');
        $this->assign('sys',$sys);
        //获取控制方法
        $request = Request::instance();
        $action = $request->action();
        $controller = $request->controller();
        $this->assign('action',($action));
        $this->assign('controller',strtolower($controller));
        define('MODULE_NAME',strtolower($controller));
        define('ACTION_NAME',strtolower($action));
    }
//获取微信access_token
    public function get_access_token($appid,$appsecret){
        //判断是否过了缓存期
        $wxUser = db('wx_user');
        $wechat = $wxUser->find();
        $expire_time = $wechat['web_expires'];
        if($expire_time > time()){
            return $wechat['web_access_token'];
        }
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$wechat['appid']}&secret={$wechat['appsecret']}";
        $return = httpRequest($url,'GET');
        $return = json_decode($return,1);
        $web_expires = time() + 7000; // 提前200秒过期
        $wxUser->where(array('id'=>$wechat['id']))->update(array('web_access_token'=>$return['access_token'],'web_expires'=>$web_expires));
        return $return['access_token'];
    }
    //获取js api_ticket
    public function getJsApiTicket(){
        if($jsapiticket=cache('jsapiticket')){
        }else{
            $info =db('wx_user')->find();
            $acces_token=$this->get_access_token($info['appid'],$info['appsecret']);
            $url="https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=".$acces_token."&type=jsapi";
            $res=httpRequest($url,'GET');
            $res=json_decode($res,true);
            $jsapiticket=$res['ticket'];
            cache('jsapiticket',$jsapiticket,7200);
        }
        return $jsapiticket;
    }
    public function getRandCode($num=16){
        $array=array(
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            '0','1','2','3','4','5','6','7','8','9'
        );
        $tmpstr='';
        for ($i=1;$i<=$num;$i++){
            $key=rand(0,count($array));
            $tmpstr.=$array[$key];
        }
        return $tmpstr;
    }
    //分享接口
    public function shareWx($url){//$url指的是分享页面当前的url
        $wxdata=array();
        $info =db('wx_user')->find();
        $wxdata['appid']=$info['appid'];
        $wxdata['time']=time();
        $wxdata['noncestr']=$this->getRandCode();
        $jsapi_ticket=$this->getJsApiTicket();
        $sinature="jsapi_ticket=".$jsapi_ticket."&noncestr=".$wxdata['noncestr']."&timestamp=".$wxdata['time']."&url=".$url;
        $wxdata['signature']=sha1($sinature);
        //获得jsapi_ticket
        $this->assign('wxdata',$wxdata);
    }

    public function _empty(){
        return $this->error('空操作，返回上次访问页面中...');
    }
}
