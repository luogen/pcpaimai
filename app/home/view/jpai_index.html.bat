<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
	<title>参与拍卖</title>
	<link rel="stylesheet" type="text/css" href="__HOME__/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="__HOME__/css/weui.min.css">
	<link rel="stylesheet" href="__HOME__/css/jquery-weui.min.css">
	<link rel="stylesheet" type="text/css" href="__HOME__/css/style.css">
       <script src="http://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
	<script>
        wx.config({
            debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
            appId: '{$wxdata['appid']}', // 必填，公众号的唯一标识
            timestamp:{$wxdata['time']} , // 必填，生成签名的时间戳
            nonceStr: '{$wxdata['noncestr']}', // 必填，生成签名的随机串
            signature: '{$wxdata['signature']}',// 必填，签名，见附录1
            jsApiList: [
                'onMenuShareAppMessage',
				'onMenuShareTimeline',
			]
        });
        wx.ready(function(){
            wx.onMenuShareTimeline({
                title: '{$goods_info['title']}', // 分享标题
                link: "{$goods_info['headurl']}{:url('Jpai/index')}?id={$goods_info['id']}", // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                imgUrl: '{$goods_info['headurl']}__PUBLIC__{$goods_info['thumb']}', // 分享图标
                success: function () {
                    // 用户确认分享后执行的回调函数
					alert('分享成功');
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                    alert('分享已取消');
                }
            });

            wx.onMenuShareAppMessage({
                title: '{$goods_info['title']}', // 分享标题
                imgUrl: '{$goods_info['headurl']}__PUBLIC__{$goods_info['thumb']}', // 分享图标
                desc: '拍卖时间:{$goods_info['starttime']|date="m月d日H:i时",###}                             起拍价:{$goods_info['startprice']}', // 分享描述
                link: "{$goods_info['headurl']}{:url('Jpai/index')}?id={$goods_info['id']}", // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                type: '', // 分享类型,music、video或link，不填默认为link
                dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                success: function () {
                    // 用户确认分享后执行的回调函数
                    alert('分享成功');
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
        });
        wx.error(function(res){

        });
    
	</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <!--<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="3"></li>-->
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
			      <img src="__PUBLIC__<?php if($goods_info['thumb']) echo $goods_info['thumb']; else echo '/static/home/images/myjpai.jpg'; ?>" alt="..." style="width:100%;height:250px;">
			      <div class="carousel-caption">
			      </div>
			    </div>
			  </div>

			  <!-- 左右点击动画 -->
			  <!-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a> -->
			</div>			
		</div>
		<div class="container-my">
			<div class="row" style=" margin-top:5px;">
				<p style="letter-spacing:2px; margin:0;font-weight:bold;">{$goods_info['title']}</p>
			</div>

			<div class="row" >
				<div class="col-xs-7" style="padding-left:0;padding-right:0;">
					<p style="font-size:12px;color:red;">目前最高价 <span style="font-size:18px;">￥{$biddenlist['0']['price']}</span>元</p>
				</div>
				<div class="col-xs-5" style="margin-top:-5px;">
					<p style="font-size:12px; color:blue; margin:0;">起拍价 ￥{$goods_info['startprice']}</p>
					<p style="font-size:12px; color:red; margin:0;">倒计时 <span id="timestr">00:59:59</span></p>
				</div>
			</div>
			<div>
				<p id="myp3" style="width:auto;overflow : hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp:3;-webkit-box-orient: vertical;">{$goods_info['goodsdes']}</p>
		               <p>拍卖企业：{$goods_info['auct_company']}</p>
                 	</div>
		</div>
		<hr style="border-top: 1px solid gray; margin:10px -15px;"/>
		<div class="container-chujia">
		       <p style="color: #27A4DF; font-size:12px;" id="cutpaimai"></p>
                     	<div class="row">
				<div class="col-xs-8" style="border-right:1px solid gray; margin-top:7px;">
                                 	<div class="row" style="color:red">
				           <span class=" glyphicon glyphicon-minus" style="padding:3px; border:1px solid red; border-radius:1px;"></span>
						<input type="number" value="{$goods_info['marupprice']}" class="price" style="background-color:#fff;text-align:center;display: inline-block; width:135px; padding:3px 0;color:#000; border-radius:2px;">
						<span class="glyphicon glyphicon-plus" style="padding:3px; border:1px solid red; border-radius:1px;"></span>
				
                                    	</div>
				</div>
				<div class="col-xs-4" style="font-size:11px;">
					<span style="display:block; width:64px; text-align:center; font-weight:bold;">加价幅度</span>
					<span style="display:block; width:62px;border:1px solid red; color:red; padding:0 4px; border-radius:3px; text-align:center; font-size:13px;">￥{$goods_info['marupprice']}元</span>
				</div>
			</div>
			<div class="row" style="margin-top:15px;">
				<div class="col-xs-8">
					<div class="row">
                                            <a href="#" class="btn btn-primary btn-lg active <?php if($goods_info['isend'] || !$goods_info['iskaipai']) echo ''; else echo 'chujia';?> <?php if($qualify['isapply']==0||$qualify['isapply']==1) echo ' hidden';?>" role="button" style="background-color: <?php if($goods_info['isend'] || !$goods_info['iskaipai']) echo 'gray'; else echo '#E70012';?>; border-color:#E70012; width:92%;">出价</a>
					<a href="#" class="btn btn-primary btn-lg active <?php if($qualify['status']) echo 'hidden';?>" role="button" style="background-color:<?php if($goods_info['isend'] || $qualify['isapply']==1) echo 'gray'; else echo '#E70012';?>; border-color:#E70012; width:92%;"data-toggle="<?php if($goods_info['isend'] || $qualify['isapply']==1) echo ''; else echo 'modal';?>" data-target="#myModal"><?php if($qualify['isapply']==0) echo '申请拍卖资格'; else echo '已参拍待审核';?></a>

                                 	</div>
				</div>
				<div class="col-xs-4">
					<span style="text-align: center;background-color:lightpink; color:gray;display:block;width:62px; font-size:15px; border-radius:3px;">您的编号<?php if($qualify['isapply']==2) echo $qualify['usernumber']; ?></span>
				</div>
			</div>
		</div>

		<div class="container" style="margin-top:15px;">
			<div class="row">
				<table class="table " style="font-size:11px;">
					{volist name="biddenlist" id="vo" mod="}

					<tr style="color:<?php if($i==1)echo 'red'; else echo 'gray'; ?>;">
						<td>编号{$vo.usernumber}</td>
						<td>{$vo.cretime|date="m-d",###}</td>
						<td>{$vo.cretime|date="H:i:s",###}</td>
						<td>￥{$vo.price}元</td>
					</tr>
					{/volist}

				</table>
			</div>
		</div>
	</div>
	

	<!-- 弹幕 -->

		<!-- Button trigger modal -->
	<!--<a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">gfjhgfj</a>-->
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
	  <div class="modal-dialog text-center" role="document">
	    <div class="modal-content " style=" margin:50% auto 0 auto; padding:12px; width:80%;">
	      <div class="modal-body" style="border:1px solid red;">
	      <form action="{:url('Home/Jpai/apply')}" method="post">
	        <div class="input-group" style="border:1px solid red; margin-bottom:15px;">
		    <span class="input-group-addon" style="border-right:0;background-color:#fff;"><img src="__HOME__/images/icon1.jpg" style="width:20px; height:20px; "></span>
		    <input type="text" class="form-control" id="inputGroupSuccess1" name="username" placeholder="请输入您的真实姓名" style="border-left:0;">
		  </div>
		  <div class="input-group" style="border:1px solid red; margin-bottom:15px;">
		    <span class="input-group-addon" style="border-right:0;background-color:#fff;"><img src="__HOME__/images/icon2.jpg" style="width:20px; height:20px; "></span>
		    <input type="text" class="form-control" name="phone" id="inputGroupSuccess1" placeholder="请输入手机号" style="border-left:0;">
		  </div>
			  <input type="hidden" name="userid" value="{$userinfo['user_id']}">
			  <input type="hidden" name="goodsid" value="{$goods_info['id']}">
		  <p style="text-align:center;"><button type="submit" class="btn btn-primary btn-md active"  style="background-color:#E70012; border-color:#E70012; width:92%;">点击申请拍卖资格</button></p>
		  </form>
	     <!--  <div class="modal-footer">
	       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	       <button type="button" class="btn btn-primary">Save changes</button>
	     </div> -->
	    </div>
	  </div>
	</div>
	</div>
	<script src="__HOME__/js/jquery.min.js"></script>
	<script src="__HOME__/js/bootstrap.min.js"></script>
	<script src="__HOME__/js/jquery-weui.min.js"></script>
	<script src="__HOME__/js/swiper.min.js"></script>
	<script src="__HOME__/js/city-picker.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
                 $('#myp3').click(function () {
				var con=$(this).text();
				alert(con);
            }); 
           $('.glyphicon-plus').click(function(){
                var a=parseInt($('.price').val())+{$goods_info['marupprice']};
                $('.price').val(a);
            });
            $('.glyphicon-minus').click(function(){
                var a=parseInt($('.price').val())-{$goods_info['marupprice']};
                if(a>={$goods_info['marupprice']})
                    $('.price').val(a);
            });

			$('.chujia').click(function(){
				var a=parseInt($('.price').val());
				if(a< parseInt({$goods_info['marupprice']})){
				    alert('不能低于最小加价幅度');
				}else{
                    //加上最高价
		    var nowmaxprice=isDingyi({$biddenlist['0']['price']});
                   if(nowmaxprice==0){
                        nowmaxprice={$goods_info['startprice']};
					}
                    a+=parseInt(nowmaxprice);
                    var usernumber='{$qualify['usernumber']}';
                    usernumber=parseInt(usernumber);
                    //var usernumber=44444;
                    var goodsid={$goods_info['id']};
                    alert("出价"+a+"元");
                    $.post(
                        "{:url('Home/Jpai/bidden')}",
                        {price:a,usernumber:usernumber,goodsid:goodsid},
                        function(data){
                            //data可以是xmlDoc,jsonObj,html,text,等等.
                            //this;//这个Ajax请求的选项配置信息，请参考jQuery.get()说到的this
                            data=JSON.parse(data);
                            if(data.status=='ok'){
                                //立即刷新页面
                                window.location.reload();
                            }else{
                                alert("出价错误，请重试");
                            }
                        },
                        "json"//这里设置了请求的返回格式为"json"
                    );
                }


			});
			//倒计时
			if({$goods_info['isend']} || ! {$goods_info['iskaipai']})
                var intDiff = 0; //倒计时总秒数量
			else
            	var intDiff = parseInt({$goods_info['cuttime']}); //倒计时总秒数量
            function timer(intDiff) {
                window.setInterval(function () {
                    var day = 0,
                        hour = 0,
                        minute = 0,
                        second = 0; //时间默认值
                    if (intDiff > 0) {
                        day = Math.floor(intDiff / (60 * 60 * 24));
                        hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
                        minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
                        second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
                    }
                    if (hour <= 9) hour = '0' + hour;
                    if (minute <= 9) minute = '0' + minute;
                    if (second <= 9) second = '0' + second;
                    var timestr=hour+":"+minute+":"+second;
                    $('#timestr').text(timestr);
                    intDiff--;
                    if(intDiff==0){
                        alert('已结束');
                        var goodsid=parseInt({$goods_info['id']});
                        var fimaxprice=isDingyi({$biddenlist['0']['price']});
                        var fistartprice={$goods_info['startprice']};
                        $.post(
                            "{:url('Home/Jpai/finish')}",
                            {goodsid:goodsid,startprice:fistartprice,maxprice:fimaxprice},
                            function(data){
                                //data可以是xmlDoc,jsonObj,html,text,等等.
                                //this;//这个Ajax请求的选项配置信息，请参考jQuery.get()说到的this
                                data=JSON.parse(data);
                                if(data.status=='ok'){
                                    //立即刷新页面
                                    window.location.reload();
                                }else{

                                }
                            },
                            "json"//这里设置了请求的返回格式为"json"
                        );
                    }
                    if(!{$goods_info['isend']}) {
			var goodsid=parseInt({$goods_info['id']});
                        $.post(
                            "{:url('Home/Jpai/isFinish')}",
                            {goodsid:goodsid},
                            function(data){
                                //data可以是xmlDoc,jsonObj,html,text,等等.
                                //this;//这个Ajax请求的选项配置信息，请参考jQuery.get()说到的this
                                data=JSON.parse(data);
                                if(data.status=='ok'){
                                    //立即刷新页面
                                    window.location.reload();
                                }else{
				//  alert(goodsid);
                                }
                            },
                            "json"//这里设置了请求的返回格式为"json"
                        );
					}
                }, 1000);
            }
            $(function () {
                timer(intDiff);
            });
            //网站定时请求是否有人加价，更新页面
			function isDingyi() {
				var a = arguments[0] ? arguments[0] : 0;
				return a;
                        }
	   function getnowTime() {
                var timestamp = Date.parse(new Date());
                timestamp = (timestamp / 1000);//这是Windows环境下
				return timestamp;
            } 
            var qualifystatus=isDingyi({$qualify['status']});
            var maxprice=isDingyi({$biddenlist['0']['price']});
			function increaseState() {
                window.setInterval(function () {
                    $.post(
                        "{:url('Home/Jpai/isIncrease')}",
                        {goodsid:{$goods_info['id']},maxprice:maxprice},
                        function(data){
                            data=JSON.parse(data);
                            if(data.status=='ok'){
                                //立即刷新页面
                                window.location.reload();
                            }else{

                            }
                        },
                        "json"//这里设置了请求的返回格式为"json"
                    );
                },2000);
            }
	  function isKaiPai(intDiff) {
                window.setInterval(function () {
                   if({$goods_info['starttime']}< getnowTime()){
					window.location.reload();}
                   else{
                      //距离拍卖会倒计时
                       var day = 0,
                           hour = 0,
                           minute = 0,
                           second = 0; //时间默认值
                       if (intDiff > 0) {
                           day = Math.floor(intDiff / (60 * 60 * 24));
                           hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
                           minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
                           second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
                       }
                       var timestr="距离拍卖行开始还有"+hour+"时"+minute+"分"+second+"秒";
                       $('#cutpaimai').text(timestr);
                       intDiff--;    
                   }
                },1000);
            }
	    var cutpaimai=parseInt({$goods_info['starttime']})-parseInt(getnowTime());
            $(function () {
                if({$goods_info['starttime']}< getnowTime())
                	increaseState();
				else
                    isKaiPai(cutpaimai);//等待出价，判断是否可以出价
            });
            //判断用户的资格申请是否通过
            function isCheak() {
                window.setInterval(function () {
                    $.post(
                        "{:url('Home/Jpai/isCheak')}",
                        {goodsid:{$goods_info['id']},userid:{$userinfo['user_id']}},
                        function(data){
                            data=JSON.parse(data);
                            if(data.status=='ok'){
                                //立即刷新页面
                                window.location.reload();
                            }else if(data.status=='bohui'){
                                alert('申请被驳回，请重新申请！');
                                window.location.reload();
                            }
                        },
                        "json"//这里设置了请求的返回格式为"json"
                    );
                },3000);
            }
            $(function () {
                 if(qualifystatus==0 && {$qualify['isapply']}==1){
                    isCheak();
				}else{

				}
            });
})
	</script>
</body>
</html>
