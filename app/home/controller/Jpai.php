<?php
namespace app\home\controller;
use think\Controller;
use think\Input;
use think\Db;
use think\Request;
class Jpai extends Common{
    public function _initialize(){
        parent::_initialize();
    }
    public function index(){
/*if(!session('userinfo')['openid']){
           $this->redirect('Weixin/accept');return ;
       }*/
        if(Request::instance()->isGet()){
            $id=intval(input('get.id'));
            if($id >0 && $id!=0){
                //网页分享实现
                $requesturl='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                $this->shareWx($requesturl);
                $goods_info=   Db::table(config('database.prefix').'salegoods')
                    ->where('id',$id)
                    ->field('id,phone,thumb1,thumb2,auctioneer,codenum,title,starttime,endprice,startprice,thumb,marupprice,cuttime,isend,goodsdes,auct_company')
                    ->find();
                $goods_info['headurl']="http://".$_SERVER['HTTP_HOST'];//www.lbmonkey.xin
               	$biddeninfo=Db::table(config('database.prefix').'bidden')
                    ->where('goodsid',$goods_info['id'])
                    ->field('id,cretime,price')
		    ->order('price desc')
                    ->select();
		$biddeninfo=$biddeninfo[0];
                //从起拍开始的倒计时
                //如果还没有人拍
                if(!$biddeninfo['cretime']){
                    $time=time();
                   $counttime= $goods_info['starttime']+$goods_info['cuttime'];
		    if($counttime>$time){
                        //还未结束，在倒计时的时间内
                        $goods_info['cuttime']=$counttime-$time;
                    }else{
                        //已经在倒计时的时间结束了
                        $goods_info['isend']=1;
			 $data['isend']=1;
                        $data['endtime']=$counttime;
                        Db::table(config('database.prefix').'salegoods')
                            ->where('id',$id)
                            ->update($data);
                     }
                }else{
                    //有人拍
                    $time=time();
                    $counttime=$goods_info['cuttime']+$biddeninfo['cretime'];
                    if($counttime>$time){
                     //   还未结束，在倒计时的时间内
                        $goods_info['cuttime']=$counttime-$time;
                    }else{
                        //已经在倒计时的时间结束了
                        $goods_info['isend']=1;
                         $data['isend']=1;
                        $data['endtime']=$counttime;
                        if($biddeninfo['price']>=$goods_info['endprice']){
                            $data['issure']=1;
                        } 
                       Db::table(config('database.prefix').'salegoods')
                            ->where('id',$id)
                            ->update($data);
                    }
                }
                $goods_info['nowtime']=time();
		      if($goods_info['starttime']>$goods_info['nowtime'])
                    $goods_info['iskaipai']=0;
                else
                    $goods_info['iskaipai']=1;
                $this->assign('goods_info',$goods_info);
                $biddenlist= Db::table('clt_bidden')->where('goodsid',$id)->order('price desc')->select();
                $this->assign('biddenlist',$biddenlist);
                $userinfo=session('userinfo');
                $qualify=Db::table(config('database.prefix').'qualify')
                    ->where('userid',$userinfo['user_id'])
                    ->where('goodsid',$id)
                    ->find();
                if(!$qualify){
                    //$qualify['usernumber']="*****";
                    $qualify['isapply']=0;
                }else if($qualify['status']==0){
                    $qualify['isapply']=1;//表示申请但是未通过
                }else if($qualify['status']==1){
                    $qualify['isapply']=2;//表示申请但是已经通过
                }
                $this->assign('qualify',$qualify);
                $this->assign('userinfo',$userinfo);
            }else{
                $this->error("错误请求id");
            }
            return $this->fetch();
        }else{
            $this->error("错误请求");
        }
    }
    //验证手机号
    function isMobile($mobile) {
        if (!is_numeric($mobile)) {
            return false;
        }
        return preg_match('#^13[\d]{9}$|^14[5,7]{1}\d{8}$|^15[^4]{1}\d{8}$|^17[0,6,7,8]{1}\d{8}$|^18[\d]{9}$#', $mobile) ? true : false;
    }
    //申请拍卖资格 表：clt_qualify
    public function apply(){
        if(Request::instance()->isPost()){
            $data['username']=input('post.username');
            $data['phone']=input('post.phone');
            if( !$this->isMobile($data['phone'])){
               $this->error('手机号格式不正确');
               exit();
           }
            //隐藏域
            $data['goodsid']=input('post.goodsid');
            $data['userid']=session('userinfo')['user_id'];
            //判断是否已经有300人
            $list=Db::table('clt_qualify')->where('goodsid',$data['goodsid'])->select();
            if(count($list)>=300){
                $this->error('对不起该拍品已经满员了');
            }else{

                $tag=Db::table('clt_qualify')
                    ->where('goodsid',$data['goodsid'])
                    ->where('userid',$data['userid'])
                    ->find();
                if($tag)
                    $this->error('等待审核中，请勿重复申请');
                $data['usernumber']=$this->shuzi($data['goodsid']);
                $data['cretime']=time();
                $id=Db::table('clt_qualify')->insertGetId($data);
                if($id){
                    //成功跳到商品详情页面
                $this->success("等待审核中");      
      //        $this->redirect('Jpai/index',['id' =>$data['goodsid']]);
                }else{
                    $this->error('出错了，请重新申请');
                }
            }
        }else{
            $this->error('请球错误');
        }
    }
    //确定一个编号
    public function shuzi($id){//递归产生一个编号
        $num1=rand(1000,2000);
        $num2=rand(2000,3000);
        $num=($num1+$num2)-rand(1,100)+rand(100,200);
        $tag=Db::table('clt_qualify')->where('usernumber',$num)->where('goodsid',$id)->select();
        if($tag){//已经存在
            $this->shuzi($id);
        }else{
            return $num;
        }
    }
    //出价 表：clt_bidden
    public function bidden(){
        if (Request::instance()->isAjax()){
            $data['usernumber']=input('post.usernumber');
            $data['goodsid']=input('post.goodsid');
            $data['price'] =input('post.price');
            $data['cretime'] =time();
            $status=Db::table(config('database.prefix').'salegoods')->where('isend',0)->where('id',$data['goodsid']);
            if($status){
                $timer=rand(100,100000);
                usleep($timer);
                $tag1=Db::table(config('database.prefix').'bidden')
                    ->where('goodsid',$data['goodsid'])
                    ->where('price',$data['price'])
                    ->find();
                if($tag1){
                    $info['status']='youren';
                }else{
                    $tag=Db::table(config('database.prefix').'bidden')->insert($data);
                    if($tag){
                        $info['status']='ok';
                    }else{
                        $info['status']='error';
                    }
                }
            }else{
                $info['status']='ended';
            }
            
              
        return json_encode($info);
        }
    }

//拍卖结束
    public function finish(){
        if (Request::instance()->isAjax()){
            $id=input('post.goodsid');
            $endprice=input('post.endprice');
            $usernumber=input('post.usernumber');
            $maxprice=input('post.maxprice');
            $data['isend']=1;
            $data['endtime']=time();
            if($maxprice>=$endprice){
                $data['issure']=1;
            }
            $tag=Db::table(config('database.prefix').'salegoods')
                ->where('id',$id)
                ->update($data);

            if($tag){
                if($maxprice==0){
                    //没有人出价流拍
                    $info['status']='noone';
                    $info['msg']="无人出价，拍品已流拍。";
                }else if ($maxprice>=$endprice){
                    $info['status']='isok';
                    $usernuminfo=Db::table(config('database.prefix').'bidden')
                        ->where('goodsid',$id)
                        ->where('price',$maxprice)
                        ->find();
                    $info['msg']="已由编号".$usernuminfo['usernumber']."以".$maxprice."元成功竞得"; 
               }else{
                    //没有大于最高价流拍
                    $info['status']='noget';
                    $info['msg']="最高应价未达保留价，拍品已流拍";
                }

            }else{
                $info['status']='error';
            }
            return json_encode($info);
        }
    }
    //判断该商品竞价是否已经结束
    public function isFinish(){
        if (Request::instance()->isAjax()) {
            $goodsid = input('post.goodsid');
            $tag = Db::table(config('database.prefix') . 'salegoods')
                ->where('id',$goodsid)
                ->where('isend', 1)
                ->find();
            if ($tag) {
                $info['status'] = 'ok';
            } else {
                $info['status'] = 'error';
            }
            return json_encode($info);
        }
    }
    //是否有人加价 ，如果最高价改变则有人加价，立即刷新页面
    public function isIncrease(){
        if (Request::instance()->isAjax()){
            $id=input('post.goodsid');
            $maxprice=input('post.maxprice');
            if($maxprice==0){
                $info['status']='error';
            }else{
                $tag=Db::table(config('database.prefix').'bidden')
                    ->where('price','>',$maxprice)
                    ->where('goodsid',$id)
                    ->find();
                if($tag){
                    $info['status']='ok';
                }else{
                    $info['status']='error';
                }
            }
            return json_encode($info);
        }
    }
    //用户的申请资格是否通过审核
    public function isCheak()
    {
        if (Request::instance()->isAjax()) {
            $goodsid = input('post.goodsid');
            $userid = input('post.userid');
            $tag = Db::table(config('database.prefix') . 'qualify')
                ->where('userid',$userid)
                ->where('goodsid',$goodsid)
                ->find();
            if ($tag) {
                if($tag['status']==1)//审核通过
                    $info['status'] = 'ok';
                else
                    $info['status'] = 'error';
            } else {
                $info['status'] = 'bohui';
            }
            return json_encode($info);
        }
    }
}
