<?php
namespace app\home\controller;
use think\Controller;
use think\Input;
use think\Db;
use think\Request;
class Index extends Common{
    public function _initialize(){
        parent::_initialize();
    }
    //本地模拟用户登录
    public function test1(){
        $data['nickname']='奋斗中人';
        $data['user_id']='2603';
        $data['openid']='oFhTxwfTaSmwz5M9hmEq1AgXpwTs';
        session(userinfo,$data);
        echo 'ok';
    }
    public function test2(){
        $data['nickname']='奋斗中人';
        $data['user_id']='2604';
        $data['openid']='oFhTxwfTaSmwz5M9hmEq1AgXpwTs';
        session(userinfo,$data);
        echo 'ok';
    }

public  function check(){
        $list= Db::table('clt_salegoods')->where('status',1)->where('isend',0)->field('id,title,starttime,startprice,thumb,cuttime')->order('starttime asc')->select();
        $time1=time();
        foreach ($list as $k => $v) {
            if ($v['starttime'] > $time1)//即将拍卖
            {}
            else
                $havelist[]=$v;
        }
        if($havelist){
            foreach ($havelist as $k=>$v){
                $biddeninfo=Db::table(config('database.prefix').'bidden')
                    ->where('goods_id',$v['id'])
                    ->field('id,cretime,price')
                    ->order('price desc')
                    ->select();
                $biddeninfo=$biddeninfo[0];
                //从起拍开始的倒计时
                //如果还没有人拍
                if(!$biddeninfo['cretime']){
                    $time=time();
                    $counttime=$v['starttime']+$v['cuttime'];
                    if($counttime>$time){
                        //还未结束，在倒计时的时间内
                        //$v['cuttime']=$counttime-$time;
                    }else{
                        //已经在倒计时的时间结束了
                        $data['isend']=1;
                        $data['endtime']=$counttime;
                   Db::table(config('database.prefix').'salegoods')
                         ->where('id',$v['id'])
                            ->update($data);
                    }
                }else{
                    //有人拍
                    $time=time();
                    $counttime=$v['cuttime']+$biddeninfo['cretime'];
                    if($counttime>$time){
                        //还未结束，在倒计时的时间内
                        //$v['cuttime']=$counttime-$time;
                    }else{
                        //已经在倒计时的时间结束了
                      //  $v['isend']=1;
                        $data['isend']=1;
                        $data['endtime']=$counttime;               
                        if($biddeninfo['price']>=$v['price']){
                            $data['issure']=1;
                        }
                        Db::table(config('database.prefix').'salegoods')
                            ->where('id',$v['id'])
                            ->update($data);
                    }
                }
            }
        }
    }
    public function index(){
/*if(!session('userinfo')['openid']){
          $this->redirect('Weixin/accept');
       }*/
     $this->check();
         //前提条件是已经发布的情况下
        //未结束的开始时间小于现在的为正在拍卖
        //未结束的排名时间大于现在的为即将拍卖
        if(Request::instance()->isPost()){
            $msg=input('post.key');
            $list=Db::table('clt_salegoods')
            ->where('status',1)
             ->where('id','eq',$msg)
            ->whereOr('title','like',"%$msg%")
            ->field('id,title,starttime,startprice,thumb,isend')
            ->order('endtime asc')
            ->select();
        }else{
               $list= Db::table('clt_salegoods')
                   ->where('status',1)
                   ->where('isend',0)
                   ->field('id,title,starttime,startprice,thumb,isend')
                   ->order('starttime asc')
                   ->select();
        }
 
       $time=time();
foreach ($list as $k => $v) {
            if ($v['starttime'] > $time)//即将拍卖
            {
                $soonlist[] = $v;
                $startlist[]=$v['starttime'];
            }
            else
                $havelist[]=$v;
        }
//var_dump(json_encode($startlist));
$this->assign('startstr',json_encode($startlist));        
$this->assign('havelist',$havelist);
        $this->assign('soonlist',$soonlist);
        return $this->fetch();
    }
 //娱乐版
    public function amusement(){
        return $this->fetch();
    }
    //联系我们
    public function contact(){
        return $this->fetch();
    }

}
